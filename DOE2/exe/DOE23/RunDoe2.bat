rem @echo off

REM %1 is weather file
REM %2 is the INP file root
REM %3 is SimFiles PATH
REM %4 is Flag for file save option
REM   ALL, 




REM check weather file
:GetWth  See if weather specified
    if not exist ..\..\weather\%1.bin  goto :ChkWth
    echo ##############################    WEATHER: %1.bin
    copy ..\..\weather\%1.bin  WEATHER.bin

REM copy weather file
:ChkWth
if exist WEATHER.bin  goto :SetupInp
    echo ????????????????????????????  Weather not specified
    echo ????????????????????????????  Available weather files:
    dir ..\..\weather\*.bin /w
rem    pause
    goto :End

:SetupInp
REM delete existing output files
   if exist *.tmp  del *.tmp
   if exist *.out  del *.out
   if exist *.log  del *.log

COPY %3%2.inp input2.tmp


:DOEBDL
    echo                                DOEBDL: Release 32-bit
    if not exist DOEBDL.exe  goto :Missing
    DOEBDL.exe
    set errlev=%errorlevel%

echo ERRORLEVEL=%errorlevel% >> DOEBDL.LOG
ren DOEBDL.LOG %2-BDL.LOG
move /Y %2-BDL.LOG %3

if "%4"=="ALL" ren  DOEBDL.out  %2.bdl
if "%4"=="ALL" move /Y %2.bdl %3

    if exist BDLKEY.tmp  del BDLKEY.tmp
    if exist BDLLIB.tmp  del BDLLIB.tmp

    if not %errlev%==1 goto :DOESIM
    echo ???????????????????????????????????? BDL ERRORS: %2.inp
    goto :End

:DOESIM
    echo                                                         DOESIM: Release 32-bit
    if not exist DOESIM.exe  goto :End
    DOESIM.exe
 
    if not exist DOESIM.out echo Check bdl file for errors >> DOESIM.out
if "%4"=="ALL" ren DOESIM.out  %2.sim
if "%4"=="ALL" move /Y %2.sim %3

ren DOEREP.lrp %2.lrp
move /Y %2.lrp %3
ren DOEREP.srp %2.srp
move /Y %2.srp %3
ren DOEHRREP.sin %2.sin
move /Y %2.sin %3
ren DOESIM.LOG %2-SIM.LOG
move /Y %2-SIM.LOG %3
ren CEC2_01.dat %2-HRLY.TXT
move /Y %2-HRLY.TXT %3

rem copy DOEREP.lrp %1%2.lrp
rem copy DOEREP.erp %1%2.erp
rem copy DOEHRREP.lin %1%2.lin
rem copy DOEHRREP.bin %1%2.bin
rem copy CEC*.dat %1%2.dat

del *.tmp
del *.out
del *.log
del *.lrp
del *.erp
del *.lin
del DOEHRREP.bin
del WEATHER.bin
del fort.*

:End

if exist %mc3resdir%current_sim_specs.csv (
echo .open %mc3dbdir%%mc3dbname% > temp.txt
echo .mode csv >> temp.txt
echo delete from current_sim_specs; >> temp.txt
echo .import %mc3resdir%/current_sim_specs.csv current_sim_specs >> temp.txt

echo attach database '%mc3resdb%' as MC3sim_results; >>temp.txt
echo .read %mc3querydir%DeleteFromAnnual.sql >>temp.txt
echo .read %mc3querydir%DeleteFromHourly.sql >>temp.txt
echo .read %mc3querydir%DeleteFromSizDat.sql >>temp.txt

echo .mode list >>temp.txt
echo .output %mc3resdir%SimParams.txt >>temp.txt
echo .read %mc3querydir%Get_Tech_Params_fmt.sql >>temp.txt

echo .mode csv >>temp.txt
echo .header on >>temp.txt
echo .output %mc3resdir%SizingConfig.csv >>temp.txt
echo .read %mc3querydir%get_current_siz_config.sql >>temp.txt

echo .mode csv >>temp.txt
echo .output %mc3resdir%result_spec_annual.csv >>temp.txt
echo .read %mc3querydir%Get_Result_Spec_Annual.sql >> temp.txt

%mc3dbdir%SQLite3.exe < temp.txt
) else (
rem ERROR: current_sim_spec.csv file does not exist
pause
)
pause

echo .open %mc3dbdir%%mc3dbname% > temp.txt
echo .mode csv >> temp.txt
echo .header on >> temp.txt
echo .output %mc3resdir%All_Measures.csv >> temp.txt
echo SELECT * FROM measure_matrix_ip WHERE MeasureID in (SELECT measure_id from msrs2proc WHERE UPPER(include) in ('1','T','TRUE')); >> temp.txt
%mc3dbdir%SQLite3.exe < temp.txt

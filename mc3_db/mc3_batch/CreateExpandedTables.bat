%mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc1-MeasApp.sql
%mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc2-TechInit.sql
%mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc3-Parameters.sql
%mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc4-VariTech.sql
%mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc5-MeasMatrix.sql
%mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc6-MeasMatIP.sql
%mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc7-TechMatrix.sql
rem %mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc8-ParamMatrix.sql may not need this anymore
%mc3dbdir%SQLite3.exe %mc3dbdir%%mc3dbname% < %mc3querydir%expProc9-MsrTechRuns.sql

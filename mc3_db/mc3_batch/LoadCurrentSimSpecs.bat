if exist %mc3resdir%current_sim_specs.csv (
echo .open %mc3dbdir%%mc3dbname% > temp.txt
echo .mode csv >> temp.txt
echo delete from current_sim_specs; >> temp.txt
echo .import %mc3resdir%/current_sim_specs.csv current_sim_specs >> temp.txt
%mc3dbdir%SQLite3.exe < temp.txt
) else (
rem ERROR: current_sim_spec.csv file does not exist
)

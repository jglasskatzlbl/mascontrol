if exist %mc3resdir%sim_hourly_eu.csv (
echo .open %mc3resdb% > temp.txt
echo .mode csv >> temp.txt
echo .import %mc3resdir%sim_hourly_eu.csv sfm_hourly_eu >> temp.txt
%mc3dbdir%SQLite3.exe < temp.txt
) else (
rem ERROR: sim_hourly_eu.csv file does not exist
)

if exist %mc3resdir%Run_Measures.csv (
echo .open %mc3dbdir%%mc3dbname% > temp.txt
echo .mode csv >> temp.txt
echo .header on >> temp.txt
echo DROP TABLE IF EXISTS current_msr_mat; >> temp.txt
echo .import %mc3resdir%Run_Measures.csv current_msr_mat >> temp.txt
echo .read %mc3querydir%expProc10-SimRuns.sql >> temp.txt
echo attach database '%mc3resdb%' as MC3sim_results; >>temp.txt
echo .read %mc3querydir%create_siz_tech_sim_runs.sql >> temp.txt
echo .mode csv >>temp.txt
echo .output %mc3resdir%siz_sim_runs.csv >>temp.txt
echo SELECT * from siz_sim_runs; >>temp.txt
echo .mode csv >>temp.txt
echo .output %mc3resdir%tech_sim_runs.csv >>temp.txt
echo SELECT * from tech_sim_runs; >>temp.txt
%mc3dbdir%SQLite3.exe < temp.txt
) else (
rem ERROR: Run_Measures.csv file does not exist
)

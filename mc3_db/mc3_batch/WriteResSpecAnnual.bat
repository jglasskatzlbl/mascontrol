echo .open %mc3dbdir%%mc3dbname% > temp.txt
echo attach database '%mc3resdb%' as MC3sim_results; >>temp.txt
echo .mode csv >>temp.txt
echo .output %mc3resdir%result_spec_annual.csv >>temp.txt
echo .read %mc3querydir%Get_Result_Spec_Annual.sql >> temp.txt
%mc3dbdir%SQLite3.exe < temp.txt
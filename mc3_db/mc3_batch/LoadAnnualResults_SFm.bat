if exist %mc3resdir%sim_result_annual.csv (
echo .open %mc3resdb% > temp.txt
echo .mode csv >> temp.txt
echo .import %mc3resdir%sim_result_annual.csv sfm_annual >> temp.txt
%mc3dbdir%SQLite3.exe < temp.txt
) else (
rem ERROR: sim_result_annual.csv file does not exist
)

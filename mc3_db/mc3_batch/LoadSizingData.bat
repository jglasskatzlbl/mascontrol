if exist %mc3resdir%sim_result_sizing.csv (
echo .open %mc3resdb% > temp.txt
echo .mode csv >> temp.txt
echo .import %mc3resdir%sim_result_sizing.csv sim_sizdat >> temp.txt
%mc3dbdir%SQLite3.exe < temp.txt
) else (
rem ERROR: sim_result_sizing.csv file does not exist
)

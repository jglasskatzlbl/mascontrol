--Create expanded "Parameters" TABLE based on the Support tables that begin with "expBldg"
DROP TABLE IF EXISTS "expParameters";
CREATE TABLE "expParameters" AS 
SELECT DISTINCT 'IP' as "TechID",
        "Parameters"."BldgType",
        "Parameters"."VintYear",
        "expBldgLoc"."BldgLoc",
        "Parameters"."BldgHVAC",
        "ParamID", 
        "ParamVal"
FROM "Parameters"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "Parameters"."BldgLoc"
WHERE "Parameters"."BldgType" <> 'Any'
  and "Parameters"."VintYear" <> 'Any'
  and "Parameters"."BldgHVAC" <> 'Any'

UNION
SELECT DISTINCT 'IP' as "TechID",
        "Parameters"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "ParamID", 
        "ParamVal"
FROM "Parameters"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "Parameters"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "Parameters"."BldgType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "Parameters"."BldgLoc"
WHERE "Parameters"."BldgType" <> 'Any'
  and "Parameters"."VintYear" = 'Any'
  and "Parameters"."BldgHVAC" = 'Any'

UNION
SELECT DISTINCT 'IP' as "TechID",
        "Parameters"."BldgType",
        "Parameters"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "ParamID", 
        "ParamVal"
FROM "Parameters"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "Parameters"."BldgType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "Parameters"."BldgLoc"
WHERE "Parameters"."BldgType" <> 'Any'
  and "Parameters"."VintYear" <> 'Any'
  and "Parameters"."BldgHVAC" = 'Any'

UNION
SELECT DISTINCT 'IP' as "TechID",
        "Parameters"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "Parameters"."BldgHVAC",
        "ParamID", 
        "ParamVal"
FROM "Parameters"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "Parameters"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "Parameters"."BldgLoc"
WHERE "Parameters"."BldgType" <> 'Any'
  and "Parameters"."VintYear" = 'Any'
  and "Parameters"."BldgHVAC" <> 'Any'

UNION
SELECT DISTINCT 'IP' as "TechID",
        "expBldgType"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "ParamID", 
        "ParamVal"
FROM "Parameters"
JOIN "expBldgType" on "Parameters"."Sector"= "expBldgType"."Sector"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "Parameters"."BldgLoc"
WHERE "Parameters"."BldgType" = 'Any'
  and "Parameters"."VintYear" = 'Any'
  and "Parameters"."BldgHVAC" = 'Any'

UNION
SELECT DISTINCT 'IP' as "TechID",
        "expBldgType"."BldgType",
        "Parameters"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "ParamID", 
        "ParamVal"
FROM "Parameters"
JOIN "expBldgType" on "Parameters"."Sector"= "expBldgType"."Sector"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "Parameters"."BldgLoc"
WHERE "Parameters"."BldgType" = 'Any'
  and "Parameters"."VintYear" <> 'Any'
  and "Parameters"."BldgHVAC" = 'Any'

UNION
SELECT DISTINCT 'IP' as "TechID",
        "expBldgType"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "Parameters"."BldgHVAC",
        "ParamID", 
        "ParamVal"
FROM "Parameters"
JOIN "expBldgType" on "Parameters"."Sector"= "expBldgType"."Sector"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "Parameters"."BldgLoc"
WHERE "Parameters"."BldgType" = 'Any'
  and "Parameters"."VintYear" = 'Any'
  and "Parameters"."BldgHVAC" <> 'Any'

UNION
SELECT DISTINCT 'IP' as "TechID",
        "expBldgType"."BldgType",
        "Parameters"."VintYear",
        "expBldgLoc"."BldgLoc",
        "Parameters"."BldgHVAC",
        "ParamID", 
        "ParamVal"
FROM "Parameters"
JOIN "expBldgType" on "Parameters"."Sector"= "expBldgType"."Sector"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "Parameters"."BldgLoc"
WHERE "Parameters"."BldgType" = 'Any'
  and "Parameters"."VintYear" <> 'Any'
  and "Parameters"."BldgHVAC" <> 'Any';

DROP INDEX IF EXISTS "main"."expar_applic";
CREATE INDEX "main"."expar_applic"
ON "expParameters" ("BldgType" ASC, "VintYear" ASC, "BldgLoc" ASC, "BldgHVAC" ASC);

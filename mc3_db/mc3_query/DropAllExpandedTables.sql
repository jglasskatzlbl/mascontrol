-- This will delete all of the  files that are created by the expProc* queries.
-- To minimize database size, one must VACUUM the database as well.
DROP TABLE IF EXISTS "expMeasApplic";
DROP TABLE IF EXISTS "expTechInit";
DROP TABLE IF EXISTS "expParameters";
DROP TABLE IF EXISTS "expVariTech";
DROP TABLE IF EXISTS "measure_matrix";
DROP TABLE IF EXISTS "measure_matrix_ip";
DROP TABLE IF EXISTS "tech_matrix_ip";
DROP TABLE IF EXISTS "param_matrix";
DROP TABLE IF EXISTS "msr_tech_runs";
DROP TABLE IF EXISTS "current_msr_mat";
DROP TABLE IF EXISTS "sim_runs";
DELETE from siz_sim_runs;
DELETE from tech_sim_runs;
VACUUM;

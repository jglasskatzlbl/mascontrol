-- Get the Technology parameters that are macros, formatted for DOE2:
SELECT ' ##set1 '||paramid||' '||paramval||src as "Macro"
FROM current_tech_params
WHERE macro = 1

--Create expanded "VariTech" TABLE based on the Support tables that begin with "expBldg"
DROP TABLE IF EXISTS "expVariTech";
CREATE TABLE "expVariTech" AS 
SELECT "VariTech"."VariTechID",
        "VariTech"."BldgType",
        "VariTech"."VintYear",
        "expBldgLoc"."BldgLoc",
        "VariTech"."BldgHVAC",
        "TechID"
FROM "VariTech"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "VariTech"."BldgLoc"
WHERE "VariTech"."BldgType" <> 'Any'
  and "VariTech"."VintYear" <> 'Any'
  and "VariTech"."BldgHVAC" <> 'Any'

UNION
SELECT "VariTech"."VariTechID",
        "VariTech"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "TechID"
FROM "VariTech"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "VariTech"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
--JOIN "expBldgHVAC" on "expBldgHVAC"."HVACType" = "expBldg2VintHVAC"."HVACType"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "VariTech"."BldgType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "VariTech"."BldgLoc"
WHERE "VariTech"."BldgType" <> 'Any'
  and "VariTech"."VintYear" = 'Any'
  and "VariTech"."BldgHVAC" = 'Any'

UNION
SELECT "VariTech"."VariTechID",
        "VariTech"."BldgType",
        "VariTech"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "TechID"
FROM "VariTech"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "VariTech"."BldgType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "VariTech"."BldgLoc"
WHERE "VariTech"."BldgType" <> 'Any'
  and "VariTech"."VintYear" <> 'Any'
  and "VariTech"."BldgHVAC" = 'Any'

UNION
SELECT "VariTech"."VariTechID",
        "VariTech"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "VariTech"."BldgHVAC",
        "TechID"
FROM "VariTech"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "VariTech"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "VariTech"."BldgLoc"
WHERE "VariTech"."BldgType" <> 'Any'
  and "VariTech"."VintYear" = 'Any'
  and "VariTech"."BldgHVAC" <> 'Any'

UNION
SELECT "VariTech"."VariTechID",
        "expBldgType"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "TechID"
FROM "VariTech"
JOIN "expBldgType" on "VariTech"."Sector"= "expBldgType"."Sector"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "VariTech"."BldgLoc"
WHERE "VariTech"."BldgType" = 'Any'
  and "VariTech"."VintYear" = 'Any'
  and "VariTech"."BldgHVAC" = 'Any'

UNION
SELECT "VariTech"."VariTechID",
        "expBldgType"."BldgType",
        "VariTech"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "TechID"
FROM "VariTech"
JOIN "expBldgType" on "VariTech"."Sector"= "expBldgType"."Sector"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "VariTech"."BldgLoc"
WHERE "VariTech"."BldgType" = 'Any'
  and "VariTech"."VintYear" <> 'Any'
  and "VariTech"."BldgHVAC" = 'Any'

UNION
SELECT "VariTech"."VariTechID",
        "expBldgType"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "VariTech"."BldgHVAC",
        "TechID"
FROM "VariTech"
JOIN "expBldgType" on "VariTech"."Sector"= "expBldgType"."Sector"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "VariTech"."BldgLoc"
WHERE "VariTech"."BldgType" = 'Any'
  and "VariTech"."VintYear" = 'Any'
  and "VariTech"."BldgHVAC" <> 'Any'

UNION
SELECT "VariTech"."VariTechID",
        "expBldgType"."BldgType",
        "VariTech"."VintYear",
        "expBldgLoc"."BldgLoc",
        "VariTech"."BldgHVAC",
        "TechID"
FROM "VariTech"
JOIN "expBldgType" on "VariTech"."Sector"= "expBldgType"."Sector"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "VariTech"."BldgLoc"
WHERE "VariTech"."BldgType" = 'Any'
  and "VariTech"."VintYear" <> 'Any'
  and "VariTech"."BldgHVAC" <> 'Any'
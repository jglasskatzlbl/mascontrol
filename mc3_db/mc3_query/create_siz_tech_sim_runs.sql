-- Create the set of Sizing runs that must exist to support Tech runs (which are all of the rows in the Sim-Runs.csv) with an indication of existing results, 
--  include the additional information of weather file name, Peak period start hour, CoolType, HeatType, End-Use Var (for hourly results), Normalizing Unit:
-- Create the set of Sizing runs that must exist to support Tech runs (which are all of the rows in the Sim-Runs.csv) with an indication of existing results:
DELETE from siz_sim_runs;
INSERT INTO siz_sim_runs 

-- Include those TechIDs that are referenced as the SizingID in a Tech run:
Select DISTINCT SizingID,'Save',sim_runs.BldgType,sim_runs.BldgVint,sim_runs.BldgLoc,sim_runs.BldgHVAC,tstat,0,0,
                WthFileName,TemplateFile,PkPerFirstHr,CoolType,HeatType,"None","None",
                sim_runs.EU_HrRepVar,Any_BldgType,Any_BldgVint,Any_BldgLoc,Any_BldgHVAC,Any_tstat,DOE2Ver,ResultGrp
FROM sim_runs 
JOIN DEER_fClimate on sim_runs.BldgLoc = DEER_fClimate.BldgLoc
JOIN DEER_fBldgTypeHvacType on sim_runs.BldgType = DEER_fBldgTypeHvacType.BldgType and sim_runs.BldgHVAC = DEER_fBldgTypeHvacType.BldgHVAC
JOIN DEER_fHVAC on sim_runs.BldgHVAC = DEER_fHVAC.BldgHVAC
JOIN DEER_fBldgType on DEER_fBldgType.BldgType = sim_runs.BldgType
JOIN EU_RepSpecs on sim_runs.EU_HrRepVar = EU_RepSpecs.EU_HrRepVar and DEER_fBldgType.SectorCode = EU_RepSpecs.Sector
 WHERE 
  SizingID not in ('None','Save') and 
  sim_runs.SizingID||sim_runs.BldgType||sim_runs.BldgVint||sim_runs.BldgLoc||sim_runs.BldgHVAC||sim_runs.tstat NOT IN (SELECT TechID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat from MC3sim_results.sim_annual)
  AND sim_runs.SizingID||sim_runs.BldgType||sim_runs.BldgVint||sim_runs.BldgLoc||sim_runs.BldgHVAC||tstat NOT IN (SELECT TechID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat from sim_runs WHERE SizingID in ('Save'))

UNION
Select DISTINCT SizingID,'Save',sim_runs.BldgType,sim_runs.BldgVint,sim_runs.BldgLoc,sim_runs.BldgHVAC,tstat,1,0,
                WthFileName,TemplateFile,PkPerFirstHr,CoolType,HeatType,"None","None",
                sim_runs.EU_HrRepVar,Any_BldgType,Any_BldgVint,Any_BldgLoc,Any_BldgHVAC,Any_tstat,DOE2Ver,ResultGrp
FROM sim_runs 
JOIN DEER_fClimate on sim_runs.BldgLoc = DEER_fClimate.BldgLoc
JOIN DEER_fBldgTypeHvacType on sim_runs.BldgType = DEER_fBldgTypeHvacType.BldgType and sim_runs.BldgHVAC = DEER_fBldgTypeHvacType.BldgHVAC
JOIN DEER_fHVAC on sim_runs.BldgHVAC = DEER_fHVAC.BldgHVAC
JOIN DEER_fBldgType on DEER_fBldgType.BldgType = sim_runs.BldgType
JOIN EU_RepSpecs on sim_runs.EU_HrRepVar = EU_RepSpecs.EU_HrRepVar and DEER_fBldgType.SectorCode = EU_RepSpecs.Sector
 WHERE 
  SizingID not in ('None','Save') and 
  sim_runs.SizingID||sim_runs.BldgType||sim_runs.BldgVint||sim_runs.BldgLoc||sim_runs.BldgHVAC||sim_runs.tstat IN (SELECT TechID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat from MC3sim_results.sim_annual)
  AND sim_runs.SizingID||sim_runs.BldgType||sim_runs.BldgVint||sim_runs.BldgLoc||sim_runs.BldgHVAC||tstat NOT IN (SELECT TechID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat from sim_runs WHERE SizingID in ('Save'))


-- Include those Tech Runs that have the SizingID = 'Save'
UNION  
Select DISTINCT sim_runs.TechID,sim_runs.SizingID,sim_runs.BldgType,sim_runs.BldgVint,sim_runs.BldgLoc,sim_runs.BldgHVAC,tstat,0,0,
      WthFileName,TemplateFile,PkPerFirstHr,CoolType,HeatType,
      sim_runs.NormUnit,coalesce(NormSrc,"None"),
      sim_runs.EU_HrRepVar,Any_BldgType,Any_BldgVint,Any_BldgLoc,Any_BldgHVAC,Any_tstat,DOE2Ver,ResultGrp
FROM sim_runs 
JOIN DEER_fClimate on sim_runs.BldgLoc = DEER_fClimate.BldgLoc
JOIN DEER_fBldgTypeHvacType on sim_runs.BldgType = DEER_fBldgTypeHvacType.BldgType and sim_runs.BldgHVAC = DEER_fBldgTypeHvacType.BldgHVAC
JOIN DEER_fHVAC on sim_runs.BldgHVAC = DEER_fHVAC.BldgHVAC
JOIN DEER_fBldgType on DEER_fBldgType.BldgType = sim_runs.BldgType
JOIN EU_RepSpecs on sim_runs.EU_HrRepVar = EU_RepSpecs.EU_HrRepVar and DEER_fBldgType.SectorCode = EU_RepSpecs.Sector
JOIN NormUnitDef on NormUnitDef.NormUnit = sim_runs.NormUnit
 WHERE 
  SizingID = 'Save' and 
  sim_runs.TechID||sim_runs.BldgType||sim_runs.BldgVint||sim_runs.BldgLoc||sim_runs.BldgHVAC||tstat NOT IN (SELECT TechID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat from MC3sim_results.sim_annual)

UNION
Select DISTINCT sim_runs.TechID,sim_runs.SizingID,sim_runs.BldgType,sim_runs.BldgVint,sim_runs.BldgLoc,sim_runs.BldgHVAC,tstat,1,0,
       WthFileName,TemplateFile,PkPerFirstHr,CoolType,HeatType,
       sim_runs.NormUnit,coalesce(NormSrc,"None"),
       sim_runs.EU_HrRepVar,Any_BldgType,Any_BldgVint,Any_BldgLoc,Any_BldgHVAC,Any_tstat,DOE2Ver,ResultGrp
FROM sim_runs 
JOIN DEER_fClimate on sim_runs.BldgLoc = DEER_fClimate.BldgLoc
JOIN DEER_fBldgTypeHvacType on sim_runs.BldgType = DEER_fBldgTypeHvacType.BldgType and sim_runs.BldgHVAC = DEER_fBldgTypeHvacType.BldgHVAC
JOIN DEER_fHVAC on sim_runs.BldgHVAC = DEER_fHVAC.BldgHVAC
JOIN DEER_fBldgType on DEER_fBldgType.BldgType = sim_runs.BldgType
JOIN EU_RepSpecs on sim_runs.EU_HrRepVar = EU_RepSpecs.EU_HrRepVar and DEER_fBldgType.SectorCode = EU_RepSpecs.Sector
JOIN NormUnitDef on NormUnitDef.NormUnit = sim_runs.NormUnit
 WHERE 
  SizingID = 'Save' and 
  sim_runs.TechID||sim_runs.BldgType||sim_runs.BldgVint||sim_runs.BldgLoc||sim_runs.BldgHVAC||tstat IN (SELECT TechID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat from MC3sim_results.sim_annual);

DELETE from tech_sim_runs;
INSERT INTO tech_sim_runs 
-- Create the set of Technology Runs, which are all of the rows in the Sim-Runs.csv, with an indication of existing results:
Select DISTINCT sim_runs.TechID,sim_runs.SizingID,sim_runs.BldgType,sim_runs.BldgVint,sim_runs.BldgLoc,sim_runs.BldgHVAC,tstat,0,0,
       WthFileName,TemplateFile,PkPerFirstHr,CoolType,HeatType,
       sim_runs.NormUnit,coalesce(NormSrc,"None"),
       sim_runs.EU_HrRepVar,Any_BldgType,Any_BldgVint,Any_BldgLoc,Any_BldgHVAC,Any_tstat,DOE2Ver,ResultGrp
FROM sim_runs 
JOIN DEER_fClimate on sim_runs.BldgLoc = DEER_fClimate.BldgLoc
JOIN DEER_fBldgTypeHvacType on sim_runs.BldgType = DEER_fBldgTypeHvacType.BldgType and sim_runs.BldgHVAC = DEER_fBldgTypeHvacType.BldgHVAC
JOIN DEER_fHVAC on sim_runs.BldgHVAC = DEER_fHVAC.BldgHVAC
JOIN DEER_fBldgType on DEER_fBldgType.BldgType = sim_runs.BldgType
JOIN EU_RepSpecs on sim_runs.EU_HrRepVar = EU_RepSpecs.EU_HrRepVar and DEER_fBldgType.SectorCode = EU_RepSpecs.Sector
JOIN NormUnitDef on NormUnitDef.NormUnit = sim_runs.NormUnit
 WHERE 
  SizingID <> 'Save' and 
  sim_runs.TechID||sim_runs.SizingID||sim_runs.BldgType||sim_runs.BldgVint||sim_runs.BldgLoc||sim_runs.BldgHVAC||tstat NOT IN (SELECT TechID||SizingID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat from MC3sim_results.sim_annual)
UNION
Select DISTINCT sim_runs.TechID,sim_runs.SizingID,sim_runs.BldgType,sim_runs.BldgVint,sim_runs.BldgLoc,sim_runs.BldgHVAC,tstat,1,0,
       WthFileName,TemplateFile,PkPerFirstHr,CoolType,HeatType,
       sim_runs.NormUnit,coalesce(NormSrc,"None"),
       sim_runs.EU_HrRepVar,Any_BldgType,Any_BldgVint,Any_BldgLoc,Any_BldgHVAC,Any_tstat,DOE2Ver,ResultGrp
FROM sim_runs 
JOIN DEER_fClimate on sim_runs.BldgLoc = DEER_fClimate.BldgLoc
JOIN DEER_fBldgTypeHvacType on sim_runs.BldgType = DEER_fBldgTypeHvacType.BldgType and sim_runs.BldgHVAC = DEER_fBldgTypeHvacType.BldgHVAC
JOIN DEER_fHVAC on sim_runs.BldgHVAC = DEER_fHVAC.BldgHVAC
JOIN DEER_fBldgType on DEER_fBldgType.BldgType = sim_runs.BldgType
JOIN EU_RepSpecs on sim_runs.EU_HrRepVar = EU_RepSpecs.EU_HrRepVar and DEER_fBldgType.SectorCode = EU_RepSpecs.Sector
JOIN NormUnitDef on NormUnitDef.NormUnit = sim_runs.NormUnit
 WHERE 
  SizingID <> 'Save' and 
  sim_runs.TechID||sim_runs.SizingID||sim_runs.BldgType||sim_runs.BldgVint||sim_runs.BldgLoc||sim_runs.BldgHVAC||tstat IN (SELECT TechID||SizingID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat from MC3sim_results.sim_annual);

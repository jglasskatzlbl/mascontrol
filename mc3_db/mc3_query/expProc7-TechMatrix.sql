-- Creates the tech_matrix_ip table based on measure_matrix ip table;
--  the tech matrix ip table lists all techids for all applicability and can 
--   be compared against the tech_results table for missing/completed simulations
--  Note: techid is either a techid or a multi-techid and is the string by which results are saved.
-- Must modify to include the SizingID: based on measure_matrix_ip.SizingSrc (None, Save, Pre, Std, Msr)
-- the SizingID can be None, Save, or a TechID 
DROP TABLE IF EXISTS tech_matrix_ip;
CREATE TABLE tech_matrix_ip AS 
SELECT DISTINCT
     "PreTechID" as "TechID",
     "PreSizingID" as "SizingID",
     "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", tstat
FROM measure_matrix_ip WHERE "PreTechID" IS NOT NULL 

UNION
SELECT DISTINCT
     "StdTechID" as "TechID",
     "StdSizingID" as "SizingID",
     "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", tstat
FROM measure_matrix_ip WHERE "StdTechID" IS NOT NULL 
UNION
SELECT DISTINCT
     "MsrTechID" as "TechID",
     "MsrSizingID" as "SizingID",
     "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", tstat
FROM measure_matrix_ip WHERE "MsrTechID" IS NOT NULL 
 ORDER BY techid,SizingID,bldgtype,bldgvint,bldgloc,bldghvac,tstat

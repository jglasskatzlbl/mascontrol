DELETE FROM "MC3sim_results"."sim_hourly_eu" WHERE TechID||SizingID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat||enduse in 
(Select TechID||SizingID||
 CASE WHEN Any_BldgType = 0 THEN BldgType ELSE "Any" END||
 CASE WHEN Any_BldgVint = 0 THEN BldgVint ELSE "Any" END||
 CASE WHEN Any_BldgLoc  = 0 THEN BldgLoc  ELSE "Any" END||
 CASE WHEN Any_BldgHVAC = 0 THEN BldgHVAC ELSE "Any" END||
 CASE WHEN Any_tstat    = 0 THEN tstat    ELSE "Any" END||
 enduse as DELID FROM current_sim_specs);

DELETE FROM "MC3sim_results"."sim_hourly_wb" WHERE TechID||SizingID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat in 
(Select TechID||SizingID||BldgType||BldgVint||BldgLoc||BldgHVAC||tstat as DELID
FROM current_sim_specs) and enduse = "20";
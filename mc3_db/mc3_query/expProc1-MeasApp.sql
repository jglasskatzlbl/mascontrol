--Create expanded "MeasureApplicability" TABLE based on the Support tables that begin with "expBldg"
DROP TABLE IF EXISTS "expMeasApplic";
CREATE TABLE "expMeasApplic" AS 
SELECT "MeasureID",
        "expBldgType"."BldgType",
        "expBldgVint"."BldgVint",
        "expBldgHVAC"."BldgHVAC",
        "expBldgLoc"."BldgLoc"
FROM "MeasureApplicability"
JOIN "expBldgType" on "MeasureApplicability"."Sector"= "expBldgType"."Sector"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgLoc"  on "expBldgLoc"."CZ" = "MeasureApplicability"."BldgLoc"
JOIN msrs2proc on msrs2proc.measure_id = MeasureApplicability.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE') 
  and "MeasureApplicability"."BldgType" = 'Any'
  and "MeasureApplicability"."BldgVint" = 'Any'
  and "MeasureApplicability"."BldgHVAC" = 'Any'

UNION
SELECT "MeasureID",
        "MeasureApplicability"."BldgType",
        "expBldgVint"."BldgVint",
        "expBldgHVAC"."BldgHVAC",
        "expBldgLoc"."BldgLoc"
FROM "MeasureApplicability"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "MeasureApplicability"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "MeasureApplicability"."BldgType"
JOIN "expBldgLoc"  on "expBldgLoc"."CZ" = "MeasureApplicability"."BldgLoc"
JOIN msrs2proc on msrs2proc.measure_id = MeasureApplicability.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE')
  and "MeasureApplicability"."BldgType" <> 'Any'
  and "MeasureApplicability"."BldgVint" = 'Any'
  and "MeasureApplicability"."BldgHVAC" = 'Any'

UNION
SELECT "MeasureID",
        "MeasureApplicability"."BldgType",
        "MeasureApplicability"."BldgVint",
        "expBldgHVAC"."BldgHVAC",
        "expBldgLoc"."BldgLoc"
FROM "MeasureApplicability"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "MeasureApplicability"."BldgType"
JOIN "expBldgLoc"  on "expBldgLoc"."CZ" = "MeasureApplicability"."BldgLoc"
JOIN msrs2proc on msrs2proc.measure_id = MeasureApplicability.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE')
  and "MeasureApplicability"."BldgType" <> 'Any'
  and "MeasureApplicability"."BldgVint" <> 'Any'
  and "MeasureApplicability"."BldgHVAC" = 'Any'

UNION
SELECT "MeasureID",
        "MeasureApplicability"."BldgType",
        "expBldgVint"."BldgVint",
        "MeasureApplicability"."BldgHVAC",
        "expBldgLoc"."BldgLoc"
FROM "MeasureApplicability"
JOIN "expBldgHVAC" on ("expBldgHVAC"."BldgType" = "MeasureApplicability"."BldgType") and ("expBldgHVAC"."BldgHVAC" = "MeasureApplicability"."BldgHVAC")
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "MeasureApplicability"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgLoc"  on "expBldgLoc"."CZ" = "MeasureApplicability"."BldgLoc"
JOIN msrs2proc on msrs2proc.measure_id = MeasureApplicability.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE')
  AND "MeasureApplicability"."BldgType" <> 'Any'
  and "MeasureApplicability"."BldgVint" = 'Any'
  and "MeasureApplicability"."BldgHVAC" <> 'Any'

UNION
SELECT "MeasureID",
        "MeasureApplicability"."BldgType",
        "MeasureApplicability"."BldgVint",
        "MeasureApplicability"."BldgHVAC",
        "expBldgLoc"."BldgLoc"
FROM "MeasureApplicability"
JOIN "expBldgHVAC" on ("expBldgHVAC"."BldgType" = "MeasureApplicability"."BldgType") and ("expBldgHVAC"."BldgHVAC" = "MeasureApplicability"."BldgHVAC")
JOIN "expBldgLoc"  on "expBldgLoc"."CZ" = "MeasureApplicability"."BldgLoc"
JOIN msrs2proc on msrs2proc.measure_id = MeasureApplicability.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE')
  and "MeasureApplicability"."BldgType" <> 'Any'
  and "MeasureApplicability"."BldgVint" <> 'Any'
  and "MeasureApplicability"."BldgHVAC" <> 'Any'

UNION
SELECT "MeasureID",
        "expBldgType"."BldgType",
        "MeasureApplicability"."BldgVint",
        "expBldgHVAC"."BldgHVAC",
        "expBldgLoc"."BldgLoc"
FROM "MeasureApplicability"
JOIN "expBldgType" on "expBldgType"."Sector" = "MeasureApplicability"."Sector"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgLoc"  on "expBldgLoc"."CZ" = "MeasureApplicability"."BldgLoc"
JOIN msrs2proc on msrs2proc.measure_id = MeasureApplicability.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE')
  and "MeasureApplicability"."BldgType" = 'Any'
  and "MeasureApplicability"."BldgVint" <> 'Any'
  and "MeasureApplicability"."BldgHVAC" = 'Any'

UNION
SELECT "MeasureID",
        "expBldgType"."BldgType",
        "expBldgVint"."BldgVint",
        "MeasureApplicability"."BldgHVAC",
        "expBldgLoc"."BldgLoc"
FROM "MeasureApplicability"
JOIN "expBldgType" on "MeasureApplicability"."Sector"= "expBldgType"."Sector"
JOIN "expBldgHVAC" on ("expBldgHVAC"."BldgType" = "expBldgType"."BldgType") and ("expBldgHVAC"."BldgHVAC" = "MeasureApplicability"."BldgHVAC")
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgLoc"  on "expBldgLoc"."CZ" = "MeasureApplicability"."BldgLoc"
JOIN msrs2proc on msrs2proc.measure_id = MeasureApplicability.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE')
  and "MeasureApplicability"."BldgType" = 'Any'
  and "MeasureApplicability"."BldgVint" = 'Any'
  and "MeasureApplicability"."BldgHVAC" <> 'Any'

UNION
SELECT "MeasureID",
        "expBldgType"."BldgType",
        "MeasureApplicability"."BldgVint",
        "MeasureApplicability"."BldgHVAC",
        "expBldgLoc"."BldgLoc"
FROM "MeasureApplicability"
JOIN "expBldgType" on "MeasureApplicability"."Sector"= "expBldgType"."Sector"
JOIN "expBldgHVAC" on ("expBldgHVAC"."BldgType" = "expBldgType"."BldgType") and ("expBldgHVAC"."BldgHVAC" = "MeasureApplicability"."BldgHVAC")
JOIN "expBldgLoc"  on "expBldgLoc"."CZ" = "MeasureApplicability"."BldgLoc"
JOIN msrs2proc on msrs2proc.measure_id = MeasureApplicability.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE')
  and "MeasureApplicability"."BldgType" = 'Any'
  and "MeasureApplicability"."BldgVint" <> 'Any'
  and "MeasureApplicability"."BldgHVAC" <> 'Any'

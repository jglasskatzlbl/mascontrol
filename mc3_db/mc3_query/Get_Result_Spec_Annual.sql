SELECT Component,EntryID,FieldName
 FROM MC3sim_results.spec_annual 
 JOIN current_sim_specs
 JOIN DEER_fBldgType ON current_sim_specs.BldgType = DEER_fBldgType.BldgType
WHERE spec_annual.ResultGrp = DEER_fBldgType.ResultGrp
ORDER BY "Order"
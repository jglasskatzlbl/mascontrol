-- Creates the param_matrix table based on tech_matrix ip table;
--  the param matrix table lists all technology parameters for a given TechID and applicability (bldg, vint, loc, hvac)
--  Note: techid is either a techid or a multi-techid and is the string by which results are saved.
--        this query could be modified to run for only one particular TechID, and it would then be executed before 
--          the start of a the series of simulations for that TechID. 
DROP TABLE IF EXISTS "param_matrix";
CREATE TABLE param_matrix AS 

-- List all of the parameters associated with a TechID when the TechID is in the TechID2Param table (i.e. NOT a multi-tech):
SELECT tech_matrix_ip."TechID",'None' as "SubTechID", "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", ParamID, ParamVal, IsMacro
FROM tech_matrix_ip 
JOIN TechID2Param on tech_matrix_ip.techid = TechID2Param.TechID
WHERE tech_matrix_ip.techid not IN (Select DISTINCT MultiTechID from TechLists) and ((tech_matrix_ip.tstat = '1') or (tech_matrix_ip.tstat = '0'))

-- List all of the parameters associated with a TechID when the TechID is in the TechID column of the TechLists table:
UNION  
SELECT tech_matrix_ip."TechID", TechLists."TechID" as "SubTechID", "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", ParamID, ParamVal, IsMacro
FROM tech_matrix_ip 
JOIN TechLists on TechLists.MultiTechID = tech_matrix_ip.techid
JOIN TechID2Param on TechLists.TechID = TechID2Param.TechID
WHERE ((tech_matrix_ip.tstat = '1') or (tech_matrix_ip.tstat = '0'))
-- and tech_matrix_ip.techid IN (Select DISTINCT MultiTechID from TechLists) 

-- List all of the parameters associated with a TechID when the TechID is in the VarTechLookupID column of the TechLists table:
UNION 
SELECT tech_matrix_ip.TechID, TechLists."VariTechID" as "SubTechID", tech_matrix_ip.BldgType, tech_matrix_ip.BldgVint, tech_matrix_ip.BldgLoc, tech_matrix_ip.BldgHVAC, ParamID, ParamVal, IsMacro
FROM tech_matrix_ip 
JOIN TechLists on TechLists.MultiTechID = tech_matrix_ip.techid
JOIN expVariTech on 
       expVariTech.VariTechID = TechLists.VariTechID
   and expVariTech.BldgType = tech_matrix_ip.bldgtype
   and expVariTech.VintYear = tech_matrix_ip.bldgvint
   and expVariTech.BldgLoc  = tech_matrix_ip.bldgloc
   and expVariTech.BldgHVAC = tech_matrix_ip.bldghvac
JOIN TechID2Param on expVariTech.TechID = TechID2Param.TechID
WHERE ((tech_matrix_ip.tstat = '1') or (tech_matrix_ip.tstat = '0'));
-- and tech_matrix_ip.techid IN (Select DISTINCT MultiTechID from TechLists)

DROP INDEX IF EXISTS "main"."parmat_applic";
CREATE INDEX "main"."parmat_applic"
ON "param_matrix" ("TechID" ASC, "BldgType" ASC, "BldgVint" ASC, "BldgLoc" ASC, "BldgHVAC" ASC);

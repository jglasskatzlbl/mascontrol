-- Query to get all of the results retrieval info for sizing parameters
--   for the building type and HVAC type specified in the current_sim_specs Table.
SELECT --current_sim_specs.BldgType,current_sim_specs.BldgHVAC,
       SizingParams.PrimKey,SizingParams.SecKey,EntryID,SizingParams.ParamName,SizingID.MinValue,SizingID.Multiplier
FROM SizingParams
JOIN SizingID ON ((SizingID.CoolType = DEER_fHVAC_SubComp.CoolType and SizingID.HeatType = DEER_fHVAC_SubComp.HeatType)
                  OR (SizingID.CoolType = DEER_fHVAC_SubComp.CoolType and SizingID.HeatType = 'Any')
                  OR (SizingID.CoolType = 'Any' and SizingID.HeatType = DEER_fHVAC_SubComp.HeatType)
                  OR (SizingID.CoolType = 'Any' and SizingID.HeatType = 'Any')) 
                AND SizingID.Component = SizingParams.Component
                AND SizingID.Keyword = SizingParams.Keyword
JOIN DEER_fHVAC_SubComp on DEER_fHVAC_SubComp.BldgHVAC = current_sim_specs.BldgHVAC
                       AND SizingParams.SubType1 = DEER_fHVAC_SubComp.SubComp
JOIN current_sim_specs
WHERE SizingParams.BldgType = current_sim_specs.BldgType and 
      SizingParams.HVAC_Config = DEER_fHVAC_SubComp.HVAC_Config and 
      SizingID.DOE2ver = current_sim_specs.DOE2ver
ORDER BY ParamName
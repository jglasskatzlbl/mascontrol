-- Creates the sim_runs table based on current_msr_mat table; (which is created from user-defined Run_Measures.csv)
-- Note: current_msr_mat is a subset of measure_mat_ip table, edited by the user and read into the database from a CSV file.
--       as such, non-existent specifications will not be NULL, as they are in the measure_mat_ip table, they will be blank instead.
DROP TABLE IF EXISTS sim_runs;
CREATE TABLE sim_runs AS 
SELECT DISTINCT
       "PreTechID" as "TechID",
       "PreSizingID" as "SizingID",
       "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", tstat, EU_HrRepVar,"None" as NormUnit
FROM current_msr_mat WHERE "PreTechID" IS NOT NULL and PreTechID <> '' 

UNION
SELECT DISTINCT
       "StdTechID" as "TechID",
       "StdSizingID" as "SizingID",
       "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", tstat, EU_HrRepVar,"None" as NormUnit
FROM current_msr_mat WHERE "StdTechID" IS NOT NULL and "StdTechID" <> ''

UNION
SELECT DISTINCT
       "MsrTechID" as "TechID",
       "MsrSizingID" as "SizingID",
       "BldgType", "BldgVint", "BldgLoc", "BldgHVAC",tstat, EU_HrRepVar, NormUnit
FROM current_msr_mat WHERE "MsrTechID" IS NOT NULL and MsrTechID <> ''
 ORDER BY techid,bldgtype,bldgvint,bldgloc,bldghvac,tstat;
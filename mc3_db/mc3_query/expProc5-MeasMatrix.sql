-- Create the measure_matrix table, which specifies all required technology runs by MeasureID.  
--  this table does not check if the technology run is part of an Initialized Prototype run, which
--  can be done by "Create Measure Matrix with IPSpecs" based on this table
DROP TABLE IF EXISTS "measure_matrix";
CREATE TABLE measure_matrix AS 
 SELECT 
 "Measures"."MeasureID",
 "expMeasApplic"."BldgType",
 "expMeasApplic"."BldgVint",
 "expMeasApplic"."BldgLoc",
 "expMeasApplic"."BldgHVAC",
 "DEER_TStat"."TStatIndex" as tstat,
 COALESCE("Measures"."PreTechID","Measures"."PreMultiTech") as "PreTechID",
 COALESCE("Measures"."StdTechID","Measures"."StdMultiTech") as "StdTechID",
 COALESCE("Measures"."MsrTechID","Measures"."MsrMultiTech") as "MsrTechID",
 "Measures"."SizingSrc",
 Measures.EU_HrRepVar,
 Measures.NormUnit
 FROM "Measures" 
 JOIN "expMeasApplic" on "expMeasApplic"."MeasureID" = "Measures"."MeasureID"  
 JOIN "DEER_TStat" on "DEER_TStat"."TStatOptions" = "Measures"."TStatOptions"
 JOIN msrs2proc on msrs2proc.measure_id = Measures.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE') and
  "Measures"."PreVariTech" IS NULL and
  "Measures"."StdVariTech" IS NULL and
  "Measures"."MsrVariTech" IS NULL

UNION
SELECT 
 "Measures"."MeasureID",
 "expMeasApplic"."BldgType",
 "expMeasApplic"."BldgVint",
 "expMeasApplic"."BldgLoc",
 "expMeasApplic"."BldgHVAC",
 "DEER_TStat"."TStatIndex" as tstat,
 "expVariTech"."TechID" as "PreTechID",
 COALESCE("Measures"."StdTechID","Measures"."StdMultiTech") as "StdTechID",
 COALESCE("Measures"."MsrTechID","Measures"."MsrMultiTech") as "MsrTechID",
 "Measures"."SizingSrc",
 Measures.EU_HrRepVar,
 Measures.NormUnit
 FROM "Measures" 
JOIN "expMeasApplic" on "expMeasApplic"."MeasureID" = "Measures"."MeasureID"  
JOIN "DEER_TStat" on "DEER_TStat"."TStatOptions" = "Measures"."TStatOptions"
JOIN expBldgVint on expBldgVint.BldgVint = expMeasApplic.BldgVint  -- new
JOIN "expVariTech" 
  on "expVariTech"."VariTechID" = "Measures"."PreVariTech"
 and "expVariTech"."BldgType" = "expMeasApplic"."BldgType"
 and "expVariTech"."VintYear" = "expBldgVint"."VintYear"   --new
 and "expVariTech"."BldgLoc"  = "expMeasApplic"."BldgLoc"
 and "expVariTech"."BldgHVAC" = "expMeasApplic"."BldgHVAC"
JOIN msrs2proc on msrs2proc.measure_id = Measures.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE') and
  "Measures"."PreVariTech" IS NOT NULL and
  "Measures"."StdVariTech" IS NULL and
  "Measures"."MsrVariTech" IS NULL

UNION
SELECT 
 "Measures"."MeasureID",
 "expMeasApplic"."BldgType",
 "expMeasApplic"."BldgVint",
 "expMeasApplic"."BldgLoc",
 "expMeasApplic"."BldgHVAC",
 "DEER_TStat"."TStatIndex" as tstat,
 pre."TechID" as "PreTechID",
 std."TechID" as "StdTechID",
 COALESCE("Measures"."MsrTechID","Measures"."MsrMultiTech") as "MsrTechID",
 "Measures"."SizingSrc",
 Measures.EU_HrRepVar,
 Measures.NormUnit
FROM "Measures" 
JOIN "expMeasApplic" on "expMeasApplic"."MeasureID" = "Measures"."MeasureID"  
JOIN "DEER_TStat" on "DEER_TStat"."TStatOptions" = "Measures"."TStatOptions"
JOIN expBldgVint on expBldgVint.BldgVint = expMeasApplic.BldgVint  -- new
JOIN "expVariTech" pre
  on pre."VariTechID" = "Measures"."PreVariTech"
 and pre."BldgType" = "expMeasApplic"."BldgType"
 and pre."VintYear" = "expBldgVint"."VintYear"   --new
 and pre."BldgLoc"  = "expMeasApplic"."BldgLoc"
 and pre."BldgHVAC" = "expMeasApplic"."BldgHVAC"
JOIN "expVariTech" std
  on std."VariTechID" = "Measures"."StdVariTech"
 and std."BldgType" = "expMeasApplic"."BldgType"
 and std."VintYear" = "expBldgVint"."VintYear"  -- new
 and std."BldgLoc"  = "expMeasApplic"."BldgLoc"
 and std."BldgHVAC" = "expMeasApplic"."BldgHVAC"
JOIN msrs2proc on msrs2proc.measure_id = Measures.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE') and
  "Measures"."ShowInList" = '1' and
  "Measures"."PreVariTech" IS NOT NULL and
  "Measures"."StdVariTech" IS NOT NULL and
  "Measures"."MsrVariTech" IS NULL

UNION
SELECT 
 "Measures"."MeasureID",
 "expMeasApplic"."BldgType",
 "expMeasApplic"."BldgVint",
 "expMeasApplic"."BldgLoc",
 "expMeasApplic"."BldgHVAC",
 "DEER_TStat"."TStatIndex" as tstat,
 pre."TechID" as "PreTechID",
 COALESCE("Measures"."StdTechID","Measures"."StdMultiTech") as "StdTechID",
 msr."TechID" as "MsrTechID",
 "Measures"."SizingSrc",
 Measures.EU_HrRepVar,
 Measures.NormUnit
FROM "Measures" 
JOIN "expMeasApplic" on "expMeasApplic"."MeasureID" = "Measures"."MeasureID"  
JOIN "DEER_TStat" on "DEER_TStat"."TStatOptions" = "Measures"."TStatOptions"
JOIN expBldgVint on expBldgVint.BldgVint = expMeasApplic.BldgVint  -- new
JOIN "expVariTech" pre
  on pre."VariTechID" = "Measures"."PreVariTech"
 and pre."BldgType" = "expMeasApplic"."BldgType"
 and pre."VintYear" = "expBldgVint"."VintYear"   --new
 and pre."BldgLoc"  = "expMeasApplic"."BldgLoc"
 and pre."BldgHVAC" = "expMeasApplic"."BldgHVAC"
JOIN "expVariTech" msr
  on msr."VariTechID" = "Measures"."MsrVariTech"
 and msr."BldgType" = "expMeasApplic"."BldgType"
 and msr."VintYear" = "expBldgVint"."VintYear"  -- new
 and msr."BldgLoc"  = "expMeasApplic"."BldgLoc"
 and msr."BldgHVAC" = "expMeasApplic"."BldgHVAC"
JOIN msrs2proc on msrs2proc.measure_id = Measures.MeasureID
 WHERE 
  upper("msrs2proc".include) in ('1','T','TRUE') and
  "Measures"."PreVariTech" IS NOT NULL and
  "Measures"."StdVariTech" IS NULL and
  "Measures"."MsrVariTech" IS NOT NULL

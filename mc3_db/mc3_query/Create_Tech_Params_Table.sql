-- Clear and then load the current_tech_params table with parameters and macros:
DELETE FROM current_tech_params;
INSERT INTO current_tech_params 
SELECT TechID2Param.ParamID as "paramid", TechID2Param.ParamVal as "paramval", TechID2Param.IsMacro as "macro", '   $ Msr TechID: '||TechID2Param.TechID as "src"
FROM TechID2Param 
JOIN current_sim_specs
WHERE
  TechID2Param.TechID  = current_sim_specs.TechID
  AND TechID2Param.techid not IN (Select DISTINCT MultiTechID from TechLists) -- Note: this is always true i.e. TechID never equals MultiTechID

-- Get the Technology parameters associated with the specified TechID when the TechID is in the TechID column of the TechLists table:
UNION 
SELECT TechID2Param.ParamID, TechID2Param.ParamVal,TechID2Param.IsMacro,'   $ Msr (Multi)TechID: '||current_sim_specs.TechID as "src"
FROM TechID2Param 
JOIN current_sim_specs
JOIN TechLists on TechLists.MultiTechID = current_sim_specs.TechID
WHERE TechID2Param.TechID = TechLists.TechID

-- Get the Technology parameters associated with the specified TechID when the TechID is in the VarTechLookupID column of the TechLists table:
UNION
SELECT TechID2Param.ParamID, TechID2Param.ParamVal,TechID2Param.IsMacro,'   $ Msr (Var)TechID: '||current_sim_specs.TechID as "src"
FROM TechID2Param 
JOIN current_sim_specs
JOIN expBldgVint on expBldgVint.BldgVint = current_sim_specs.BldgVint
JOIN TechLists on TechLists.MultiTechID = current_sim_specs.TechID
JOIN expVariTech on expVariTech.VariTechID = TechLists.VariTechID AND
      expVariTech.BldgType = current_sim_specs.BldgType AND
      expVariTech.VintYear = expBldgVint.VintYear AND
      expVariTech.BldgLoc  = current_sim_specs.BldgLoc AND
      expVariTech.BldgHVAC = current_sim_specs.BldgHVAC
WHERE TechID2Param.TechID = expVariTech.TechID

-- Get the Non-Technology parameters associated with the model:
UNION
SELECT expParameters.ParamID, expParameters.ParamVal,'0' as "macro",'   $ IP Non-Tech' as "src"
FROM expParameters 
JOIN current_sim_specs 
JOIN DEER_fBldgType on DEER_fBldgType.BldgType = current_sim_specs.BldgType
JOIN expBldgVint on 
      expBldgVint.BldgVint = current_sim_specs.BldgVint and 
      expBldgVint.VintType = DEER_fBldgType.VintType
WHERE expParameters.BldgType = current_sim_specs.BldgType AND
      expParameters.VintYear = expBldgVint.VintYear AND
      expParameters.BldgLoc  = current_sim_specs.BldgLoc AND
      expParameters.BldgHVAC = current_sim_specs.BldgHVAC

-- Get the thermostat setting for residential runs:
UNION
SELECT ParamTstat.ParamID, ParamTstat.ParamVal,'0' as "macro",'   $ IP Non-Tech' as "src"
FROM ParamTstat 
JOIN current_sim_specs 
JOIN DEER_fBldgType on DEER_fBldgType.BldgType = current_sim_specs.BldgType
JOIN expBldgVint on 
      expBldgVint.BldgVint = current_sim_specs.BldgVint and 
      expBldgVint.VintType = DEER_fBldgType.VintType
WHERE ParamTstat.BldgType = current_sim_specs.BldgType AND
      ParamTstat.VintYear = expBldgVint.VintYear AND
      ParamTstat.BldgLoc  = current_sim_specs.BldgLoc AND
      ParamTstat.tstat = current_sim_specs.tstat;

-- Add the TechInit parameters that aren't already in the table:
INSERT INTO current_tech_params 
SELECT DISTINCT TechID2Param.ParamID as "paramid", TechID2Param.ParamVal as "paramval", TechID2Param.IsMacro as "macro", '   $ IP Tech: '||TechID2Param.TechID as "src"
FROM expTechInit 
JOIN TechID2Param on TechID2Param.TechID = expTechInit.TechID
JOIN current_sim_specs
JOIN expBldgVint on expBldgVint.BldgVint = current_sim_specs.BldgVint
WHERE expTechInit.BldgType = current_sim_specs.BldgType AND
      expTechInit.VintYear = expBldgVint.VintYear AND
      expTechInit.BldgLoc  = current_sim_specs.BldgLoc AND
      expTechInit.BldgHVAC = current_sim_specs.BldgHVAC AND
  -- Do not include any ParameterIDs that are part of the specified (measure) TechID (i.e. the first three queries above):
      TechID2Param.ParamID NOT IN (select paramid from current_tech_params);

-- Creates the tech_matrix_ip table based on measure_matrix ip table;
--  the tech matrix ip table lists all techids for all applicability and can 
--   be compared against the tech_results table for missing/completed simulations
--  Note: techid is either a techid or a multi-techid and is the string by which results are saved.
DROP TABLE IF EXISTS msr_tech_runs;
CREATE TABLE msr_tech_runs AS 
SELECT DISTINCT
       "PreTechID" as "TechID",
       "PreSizingID" as "SizingID",
       "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", tstat, EU_HrRepVar, NormUnit
FROM measure_matrix_ip WHERE "PreTechID" IS NOT NULL 
  and MeasureID in (select measure_id from msrs2proc where msrs2proc.include in ('t','TRUE'))

UNION
SELECT DISTINCT
       "StdTechID" as "TechID",
       "StdSizingID" as "SizingID",
       "BldgType", "BldgVint", "BldgLoc", "BldgHVAC", tstat, EU_HrRepVar, NormUnit
FROM measure_matrix_ip WHERE "StdTechID" IS NOT NULL 
  and MeasureID in (select measure_id from msrs2proc where msrs2proc.include in ('t','TRUE'))

UNION
SELECT DISTINCT
       "MsrTechID" as "TechID",
       "MsrSizingID" as "SizingID",
       "BldgType", "BldgVint", "BldgLoc", "BldgHVAC",tstat, EU_HrRepVar, NormUnit
FROM measure_matrix_ip WHERE "MsrTechID" IS NOT NULL 
  and MeasureID in (select measure_id from msrs2proc where msrs2proc.include in ('t','TRUE'))
ORDER BY techid,bldgtype,bldgvint,bldgloc,bldghvac,tstat

-- This query should only be called when the NormSrc in the current_sim_specs is "NUfromSizDat"
SELECT
 current_sim_specs.TechID,
 current_sim_specs.SizingID,
 current_sim_specs.BldgType,
 current_sim_specs.BldgVint,
 current_sim_specs.BldgLoc,
 current_sim_specs.BldgHVAC,
 current_sim_specs.tstat,
 current_sim_specs.NormUnit,
 sum(ParamVal)/Divisor as "NormVal"
From MC3sim_results.sim_sizdat
JOIN NormUnitDef on NormUnitDef.NormUnit = current_sim_specs.NormUnit
JOIN current_sim_specs
WHERE 
 ((current_sim_specs.SizingID IN ('Save','None') and MC3sim_results.sim_sizdat.TechID = current_sim_specs.TechID) OR
  (current_sim_specs.SizingID NOT IN ('Save','None') and MC3sim_results.sim_sizdat.TechID = current_sim_specs.SizingID)) 
 AND
 MC3sim_results.sim_sizdat.BldgType = current_sim_specs.BldgType AND
 MC3sim_results.sim_sizdat.BldgVint = current_sim_specs.BldgVint AND
 MC3sim_results.sim_sizdat.BldgLoc = current_sim_specs.BldgLoc AND
 MC3sim_results.sim_sizdat.BldgHVAC = current_sim_specs.BldgHVAC AND
 MC3sim_results.sim_sizdat.tstat = current_sim_specs.tstat AND
 ParamID IN (
  SELECT ParamName 
    FROM SizingParams
    JOIN current_sim_specs
    JOIN NormUnitDef on NormUnitDef.NormUnit = current_sim_specs.NormUnit
    JOIN DEER_fHVAC on DEER_fHVAC.BldgHVAC = current_sim_specs.BldgHVAC
   WHERE 
    SizingParams.BldgType  = current_sim_specs.BldgType AND
    SizingParams.HVAC_Config  = DEER_fHVAC.HVAC_Config AND
    SizingParams.SubType1  = NormUnitDef.SubType1 AND
    SizingParams.Component = NormUnitDef.Component AND
    SizingParams.Keyword   = NormUnitDef.Keyword AND
    ((NormUnitDef.SubType2 <> "NA" AND
    SizingParams.SubType2  = NormUnitDef.SubType2) OR
    NormUnitDef.SubType2 = "NA"))
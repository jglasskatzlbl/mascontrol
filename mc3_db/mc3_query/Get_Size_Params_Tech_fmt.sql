SELECT ' "'||ParamID||'" = '||ParamVal||'   $ Sizing' as "Parameter"
FROM "MC3sim_results"."sim_sizdat"
JOIN current_sim_specs
WHERE "MC3sim_results"."sim_sizdat".TechID   = current_sim_specs.SizingID AND
      "MC3sim_results"."sim_sizdat".BldgType = current_sim_specs.BldgType AND
      "MC3sim_results"."sim_sizdat".BldgVint = current_sim_specs.BldgVint AND
      "MC3sim_results"."sim_sizdat".BldgLoc  = current_sim_specs.BldgLoc AND
      "MC3sim_results"."sim_sizdat".BldgHVAC = current_sim_specs.BldgHVAC AND
      "MC3sim_results"."sim_sizdat".tstat    = current_sim_specs.tstat
ORDER BY "Parameter"
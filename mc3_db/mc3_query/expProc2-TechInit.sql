--Create expanded "TechInit" TABLE based on the Support tables that begin with "expBldg"
DROP TABLE IF EXISTS "expTechInit";
CREATE TABLE "expTechInit" AS 
SELECT "TechInit"."BldgType",
       "TechInit"."VintYear",
       "expBldgLoc"."BldgLoc",
       "TechInit"."BldgHVAC",
       "TechID"
FROM "TechInit"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "TechInit"."BldgLoc"
WHERE "TechInit"."BldgType" <> 'Any'
  and "TechInit"."VintYear" <> 'Any'
  and "TechInit"."BldgHVAC" <> 'Any'

UNION
SELECT "TechInit"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "TechID"
FROM "TechInit"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "TechInit"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = TechInit.BldgType
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "TechInit"."BldgLoc"
WHERE "TechInit"."BldgType" <> 'Any'
  and "TechInit"."VintYear" = 'Any'
  and "TechInit"."BldgHVAC" = 'Any'

UNION
SELECT "TechInit"."BldgType",
       "TechInit"."VintYear",
       "expBldgLoc"."BldgLoc",
       "expBldgHVAC"."BldgHVAC",
       "TechID"
FROM "TechInit"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = TechInit.BldgType
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "TechInit"."BldgLoc"
WHERE "TechInit"."BldgType" <> 'Any'
  and "TechInit"."VintYear" <> 'Any'
  and "TechInit"."BldgHVAC" = 'Any'

UNION
SELECT "TechInit"."BldgType",
       "expBldgVint"."VintYear",
       "expBldgLoc"."BldgLoc",
       "TechInit"."BldgHVAC",
        "TechID"
FROM "TechInit"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "TechInit"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "TechInit"."BldgLoc"
WHERE "TechInit"."BldgType" <> 'Any'
  and "TechInit"."VintYear" = 'Any'
  and "TechInit"."BldgHVAC" <> 'Any'

UNION
SELECT "expBldgType"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "TechID"
FROM "TechInit"
JOIN "expBldgType" on "TechInit"."Sector"= "expBldgType"."Sector"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = TechInit.BldgType
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "TechInit"."BldgLoc"
WHERE "TechInit"."BldgType" = 'Any'
  and "TechInit"."VintYear" = 'Any'
  and "TechInit"."BldgHVAC" = 'Any'

UNION
SELECT "expBldgType"."BldgType",
        "TechInit"."VintYear",
        "expBldgLoc"."BldgLoc",
        "expBldgHVAC"."BldgHVAC",
        "TechID"
FROM "TechInit"
JOIN "expBldgType" on "TechInit"."Sector"= "expBldgType"."Sector"
JOIN "expBldgHVAC" on "expBldgHVAC"."BldgType" = TechInit.BldgType
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "TechInit"."BldgLoc"
WHERE "TechInit"."BldgType" = 'Any'
  and "TechInit"."VintYear" <> 'Any'
  and "TechInit"."BldgHVAC" = 'Any'

UNION
SELECT "expBldgType"."BldgType",
        "expBldgVint"."VintYear",
        "expBldgLoc"."BldgLoc",
        "TechInit"."BldgHVAC",
        "TechID"
FROM "TechInit"
JOIN "expBldgType" on "TechInit"."Sector"= "expBldgType"."Sector"
JOIN "DEER_fBldgType" on "DEER_fBldgType"."BldgType" = "expBldgType"."BldgType"
JOIN "expBldgVint" on "expBldgVint"."VintType" = "DEER_fBldgType"."VintType"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "TechInit"."BldgLoc"
WHERE "TechInit"."BldgType" = 'Any'
  and "TechInit"."VintYear" = 'Any'
  and "TechInit"."BldgHVAC" <> 'Any'

UNION
SELECT "expBldgType"."BldgType",
        "TechInit"."VintYear",
        "expBldgLoc"."BldgLoc",
        "TechInit"."BldgHVAC",
        "TechID"
FROM "TechInit"
JOIN "expBldgType" on "TechInit"."Sector"= "expBldgType"."Sector"
JOIN "expBldgLoc" on "expBldgLoc"."CZ" = "TechInit"."BldgLoc"
WHERE "TechInit"."BldgType" = 'Any'
  and "TechInit"."VintYear" <> 'Any'
  and "TechInit"."BldgHVAC" <> 'Any';

DROP INDEX IF EXISTS "main"."exptech_applic";
CREATE INDEX "main"."exptech_applic"
ON "expTechInit" ("BldgType" ASC, "VintYear" ASC, "BldgLoc" ASC, "BldgHVAC" ASC);

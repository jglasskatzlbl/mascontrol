Select 
 current_sim_specs.TechID,
 current_sim_specs.SizingID,
 current_sim_specs.BldgType,
 current_sim_specs.BldgVint,
 current_sim_specs.BldgLoc,
 current_sim_specs.BldgHVAC,
 current_sim_specs.tstat,
 current_sim_specs.NormUnit,
 NormVal 
FROM NormUnitVal 
JOIN current_sim_specs
JOIN NormUnitDef on NormUnitDef.NormUnit = current_sim_specs.NormUnit
JOIN DEER_fVintage on DEER_fVintage.BldgVint = current_sim_specs.BldgVint
WHERE NormUnitVal.NormUnit = current_sim_specs.NormUnit AND
      ((NormUnitVal.BldgType = "Any")  OR (NormUnitVal.BldgType = current_sim_specs.BldgType)) AND
      ((NormUnitVal.VintYear = "Any")  OR (NormUnitVal.VintYear = DEER_fVintage.VintYear)) AND
      ((NormUnitVal.BldgLoc  = "Any")  OR (NormUnitVal.BldgLoc  = current_sim_specs.BldgLoc))
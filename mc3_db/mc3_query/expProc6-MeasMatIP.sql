-- Creates the measure_matrix_ip table based on measure_matrix table;
--  specifies the PreTechID or the StdTechID = 'IP' when the primary keys match the primary keys in the TechInit table
-- Note: techid is either a techid or a multitechid and is the string by which results are saved.
-- This table is used to calculate Measure energy impacts by specifying the technology (or IP) results that need to be compared
--  to determine the energy savings for each measure instance.
-- Only the 7 cases described below are accounted for; other scenarios could be possible with new measure definitions.
DROP TABLE IF EXISTS measure_matrix_ip;
CREATE TABLE measure_matrix_ip AS 
-- Case 1: PreTechID = NULL, StdTechID Not in IP:
SELECT MeasureID,
         BldgType,
         measure_matrix.BldgVint,
         BldgLoc,
         BldgHVAC,
         tstat,
         NULL as PreTechID,
         NULL as PreSizingID,
         StdTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'ERROR'
         WHEN SizingSrc = 'Std' THEN 'Save'
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS StdSizingID,
         MsrTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'ERROR'
         WHEN SizingSrc = 'Std' THEN StdTechID
         WHEN SizingSrc = 'Msr' THEN 'Save'
         ELSE 'None'
       END 
      END
      AS MsrSizingID,
         SizingSrc,
         EU_HrRepVar,
         NormUnit
  FROM measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint
      WHERE 
       PreTechID IS NULL and 
       StdTechID IS NOT NULL and 
       StdTechID||BldgType||VintYear||BldgLoc||BldgHVAC NOT IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as stdisip
         FROM (select BldgType, VintYear, BldgLoc, BldgHVAC, StdTechID as techid from measure_matrix  join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where StdTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) 

UNION
 -- Case 2: PreTechID = NULL, StdTechID in IP:
SELECT MeasureID,
         BldgType,
         measure_matrix.BldgVint,
         BldgLoc,
         BldgHVAC,
         tstat,
         NULL as PreTechID,
         NULL as PreSizingID,
         'IP' AS StdTechID, 
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'ERROR'
         WHEN SizingSrc = 'Std' THEN 'SAVE'
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS StdSizingID,
         MsrTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'ERROR'
         WHEN SizingSrc = 'Std' THEN 'IP'
         WHEN SizingSrc = 'Msr' THEN 'Save'
         ELSE 'None'
       END 
      END
      AS MsrSizingID,
         SizingSrc,
         EU_HrRepVar,
         NormUnit
  FROM measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint
      WHERE 
       PreTechID IS NULL and 
       StdTechID IS NOT NULL and 
       StdTechID||BldgType||VintYear||BldgLoc||BldgHVAC IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as stdisip
         FROM (select BldgType, VintYear, BldgLoc, BldgHVAC, StdTechID as techid from measure_matrix  join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where StdTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) 
  
UNION
-- Case 3: PreTechID in IP, StdTechID is NULL:
SELECT MeasureID,
         BldgType,
         measure_matrix.BldgVint,
         BldgLoc,
         BldgHVAC,
         tstat,
         'IP' AS PreTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE 
       Case 
         WHEN SizingSrc = 'Pre' THEN 'Save'
         WHEN SizingSrc = 'Std' THEN 'ERROR'
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS PreSizingID,
         NULL as StdTechID,
         NULL as StdSizingID,
         MsrTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'IP'
         WHEN SizingSrc = 'Std' THEN 'ERROR'
         WHEN SizingSrc = 'Msr' THEN 'Save'
         ELSE 'None'
       END 
      END
      AS MsrSizingID,
         SizingSrc,
         EU_HrRepVar,
         NormUnit
  FROM measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint
      WHERE 
       PreTechID IS NOT NULL and 
       PreTechID||BldgType||VintYear||BldgLoc||BldgHVAC IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as preisip
         FROM (select BldgType, VintYear, BldgLoc, BldgHVAC, PreTechID as techid from measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint
                where PreTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) AND
       StdTechID IS NULL
  
UNION
-- Case 4: PreTechID in IP, StdTechID in IP:
SELECT MeasureID,
         BldgType,
         measure_matrix.BldgVint,
         BldgLoc,
         BldgHVAC,
         tstat,
         'IP' AS PreTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE 
       Case 
         WHEN SizingSrc = 'Pre' THEN 'Save'
         WHEN SizingSrc = 'Std' THEN 'Save'
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS PreSizingID,
         'IP' AS StdTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE 
       Case 
         WHEN SizingSrc = 'Pre' THEN 'Save'
         WHEN SizingSrc = 'Std' THEN 'Save'
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS StdSizingID,
         MsrTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'IP'
         WHEN SizingSrc = 'Std' THEN 'IP'
         WHEN SizingSrc = 'Msr' THEN 'Save'
         ELSE 'None'
       END 
      END
      AS MsrSizingID,
         SizingSrc,
         EU_HrRepVar,
         NormUnit
  FROM measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint
      WHERE 
       PreTechID IS NOT NULL and 
       PreTechID||BldgType||VintYear||BldgLoc||BldgHVAC IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as preisip
         FROM (select BldgType, VintYear, BldgLoc, BldgHVAC, PreTechID as techid from measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where PreTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) AND
       StdTechID IS NOT NULL and 
       StdTechID||BldgType||VintYear||BldgLoc||BldgHVAC IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as stdisip
         FROM (select BldgType, VintYear, BldgLoc, BldgHVAC, StdTechID as techid from measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where StdTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) 

UNION
-- Case 5: PreTechID in IP, StdTechID not in IP:
SELECT MeasureID,
         BldgType,
         measure_matrix.BldgVint,
         BldgLoc,
         BldgHVAC,
         tstat,
         'IP' AS PreTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE 
       Case 
         WHEN SizingSrc = 'Pre' THEN 'Save'
         WHEN SizingSrc = 'Std' THEN StdTechID
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS PreSizingID,
         StdTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'IP'
         WHEN SizingSrc = 'Std' THEN 'Save'
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS StdSizingID,
         MsrTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'IP'
         WHEN SizingSrc = 'Std' THEN StdTechID
         WHEN SizingSrc = 'Msr' THEN 'Save'
         ELSE 'None'
       END 
      END
      AS MsrSizingID,
         SizingSrc,
         EU_HrRepVar,
         NormUnit
  FROM measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint 
      WHERE 
       PreTechID IS NOT NULL and 
       PreTechID||BldgType||VintYear||BldgLoc||BldgHVAC IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as preisip
         FROM (select bldgtype, VintYear, bldgloc,bldghvac, PreTechID as techid from measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where PreTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) AND
       StdTechID IS NOT NULL and 
       StdTechID||BldgType||VintYear||BldgLoc||BldgHVAC NOT IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as stdisip
         FROM (select bldgtype, VintYear, bldgloc,bldghvac, StdTechID as techid from measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where StdTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) 

UNION
-- Case 6: PreTechID not in IP, StdTechID is NULL:
SELECT MeasureID,
         BldgType,
         measure_matrix.BldgVint,
         BldgLoc,
         BldgHVAC,
         tstat,
         PreTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
          WHEN SizingSrc = 'Pre' THEN 'Save' 
          WHEN SizingSrc = 'Std' THEN 'ERROR'
          WHEN SizingSrc = 'Msr' THEN MsrTechID
          ELSE 'None'
       END 
      END
      AS PreSizingID,
         NULL as StdTechID,
         NULL as StdSizingID,
         MsrTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN PreTechID
         WHEN SizingSrc = 'Std' THEN 'ERROR'
         WHEN SizingSrc = 'Msr' THEN 'Save'
         ELSE 'None'
       END 
      END
      AS MsrSizingID,
         SizingSrc,
         EU_HrRepVar,
         NormUnit
  FROM measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint 
      WHERE 
       PreTechID IS NOT NULL and 
       PreTechID||BldgType||VintYear||BldgLoc||BldgHVAC NOT IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as preisip
         FROM (select bldgtype, VintYear, bldgloc,bldghvac, PreTechID as techid from measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where PreTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) AND
       StdTechID IS NULL

UNION
-- Case 7: PreTechID not in IP, StdTechID not in IP:
SELECT MeasureID,
         BldgType,
         measure_matrix.BldgVint,
         BldgLoc,
         BldgHVAC,
         tstat,
         PreTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN 'Save' 
         WHEN SizingSrc = 'Std' THEN StdTechID
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS PreSizingID,
         StdTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN CASE WHEN PreTechID = StdTechID THEN 'Save' ELSE PreTechID END
         WHEN SizingSrc = 'Std' THEN 'Save'
         WHEN SizingSrc = 'Msr' THEN MsrTechID
         ELSE 'None'
       END 
      END
      AS StdSizingID,
         MsrTechID,
      Case WHEN measure_matrix.BldgVint = 'New' THEN 
         'None' ELSE
       Case 
         WHEN SizingSrc = 'Pre' THEN PreTechID
         WHEN SizingSrc = 'Std' THEN StdTechID
         WHEN SizingSrc = 'Msr' THEN 'Save'
         ELSE 'None'
       END 
      END
      AS MsrSizingID,
         SizingSrc,
         EU_HrRepVar,
         NormUnit
  FROM measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint 
      WHERE 
       PreTechID IS NOT NULL and 
       PreTechID||BldgType||VintYear||BldgLoc||BldgHVAC NOT IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as preisip
         FROM (select bldgtype, VintYear, bldgloc,bldghvac, PreTechID as techid from measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where PreTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        ) AND
       StdTechID IS NOT NULL and 
       StdTechID||BldgType||VintYear||BldgLoc||BldgHVAC NOT IN 
        (
        SELECT techid||BldgType||VintYear||BldgLoc||BldgHVAC as stdisip
         FROM (select bldgtype, VintYear, bldgloc,bldghvac, StdTechID as techid from measure_matrix join expBldgVint on expBldgVint.BldgVint = measure_matrix.BldgVint where StdTechID IS NOT NULL
          INTERSECT
         SELECT BldgType, VintYear, BldgLoc, BldgHVAC, TechID as techid from expTechInit	
              ) a 
        )	
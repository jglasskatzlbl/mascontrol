-- Get the Technology parameters that aren't macros, formatted for DOE2:
SELECT ' "'||paramid||'" = '||paramval||src as "Parameter"
FROM current_tech_params
WHERE macro = 0 

$ ---------------------------------------------------------
$ Double-Wide Mobile Home Prototype
$ Created from the legacy DEER2001 INP file

DIAGNOSTIC ERRORS  ..

$ ---------------------------------------------------------
$              Global Parameters
$ ---------------------------------------------------------

##set1 BldgType = DMo

PARAMETER
$ Independents Set by MasGen
   BldgType = DMo
   WinShadTrans = 0.72










   ResShadeJan = 0.9
   ResShadeFeb = 0.825
   ResShadeMar = 0.7
   ResShadeApr = 0.6
   ResShadeMay = 0.5
   ResShadeJun = 0.375
   ResShadeJul = 0.3
   ResShadeAug = 0.375
   ResShadeSep = 0.5
   ResShadeOct = 0.6
   ResShadeNov = 0.7
   ResShadeDec = 0.825





$ === The following parameters are fixed for all MFm prototypes ======
   WinY = 2.5
   DoorHt = 6.7
   DoorWid = 3
   CircWid = 8   
   FlrWid = 23

   ..

$ ============================================================================80
ProjData = PROJECT-DATA
$   FLOOR-LENGTH     = { SQRT( #PA("AreaPerUnit")*#PA("AspectRatio") ) }
$   FLOOR-WIDTH      = { #PA("AreaPerUnit")/#L("FLOOR-LENGTH") }
$   LIV-RM-LENGTH       = { #L("FLOOR-LENGTH") * #PA("FracLivRm") }
$   BED-RM-LENGTH    = { #L("FLOOR-LENGTH") - #L("LIV-RM-LENGTH") }
$   TYP-WIN-WIDTH        = { (#L("AREA-PER-FLR") * #PA("WinFlrRatio")/4)
$                       / ( #PA("WinHt") * 4.5) } 
   AREA-PER-FLR = { #PA("FlrLen") * #PA("FlrWid") }
   SHAD-SHORT = 1
   SHAD-LONG  = 1
  ..


TITLE
LINE-1           = *DMo*
   ..


EntireYear = RUN-PERIOD-PD
   BEGIN-MONTH      = 1
   BEGIN-DAY        = 1
   BEGIN-YEAR       = 2009
   END-MONTH        = 12
   END-DAY          = 31
   END-YEAR         = 2009
   ..

CoolingDesignDay_7 = DESIGN-DAY
   TYPE = COOLING
   DRYBULB-HIGH = {#PA("CDD_DBHi7")}
   DRYBULB-RANGE = {#PA("CDD_DBRange7")}
   HOUR-HIGH = 15
   HOUR-LOW = 5
   WETBULB-AT-HIGH = {#PA("CDD_WB7")}
   WIND-SPEED = 0
   CLOUD-AMOUNT = 0
   MONTH = 7
   DAY = {#PA("CDD_Day7")}
   NUMBER-OF-DAYS = 1
   ..
HeatingDesignDay_1 = DESIGN-DAY
   TYPE = HEATING
   DRYBULB-HIGH = {#PA("HDD_DBHi1")}
   DRYBULB-RANGE = {#PA("HDD_DBRange1")}
   HOUR-HIGH = 15
   HOUR-LOW = 6
   WETBULB-AT-HIGH = {#PA("HDD_WB1")}
   WIND-SPEED = {#PA("HDD_WindSpd1")}
   CLOUD-AMOUNT = {#PA("HDD_CloudAmt1")}
   MONTH = 1
   DAY = {#PA("HDD_Day1")}
   NUMBER-OF-DAYS = 1
   ..

Standard_US_Holidays = HOLIDAYS        
   LIBRARY-ENTRY "US"
   ..
   

$ ---------------------------------------------------------
$              Site and Building Data
$ ---------------------------------------------------------

Site_Data = SITE-PARAMETERS 
   ALTITUDE = {#PA("Elevation")}
   ..

Build_Params = BUILD-PARAMETERS
   HOLIDAYS         = "Standard_US_Holidays"
   ..

$ ---------------------------------------------------------
$              Materials and Constructions
$ ---------------------------------------------------------

$ ------- GlassMethod must be set to "SC" in bare bones file ----------
GlassType_SC = GLASS-TYPE
   TYPE = SHADING-COEF
   SHADING-COEF = 0.7
   ..
GlassType_GTC = GLASS-TYPE
   TYPE = GLASS-TYPE-CODE
   GLASS-TYPE-CODE = {#SI(#PA("WinCode"),"GLASS-TYPE","GLASS-TYPE-CODE")}
   ..

Mat_UFictCC04Carpet01 = MATERIAL        
   TYPE       = RESISTANCE
   RESISTANCE = {#PA("FlrLen")*#PA("FlrWid")
     /(#PA("SlabF2")*(#PA("FlrLen")*2+#PA("FlrWid")*2)) - 4.333}
     ..

PARAMETER
   EWLayR = 1.48
   EWInR = 0.68
   OutR = 0.17
   Wd2b6R = 6.88
   ..

$ Assume exterior wall is 2x6 construction
$   Wood2by6R = 5.5 * 1.25 = 6.88
$ If ins thickness < 5.5 inch, then need to add air gap resistance = 0.9
$ For Uo calc, Rinside = 0.68, Routside = 0.17
$ Frame factor is 25%

Mat_InsLay_EWall = MATERIAL        
   TYPE             = RESISTANCE
   RESISTANCE       = {if(#PA("EWallUo") > 0) then
       1/#PA("EWallUo") - #PA("EWLayR")-#PA("EWInR")-#PA("OutR") 
     else if (#PA("EWInsR")/#PA("EWRperInch") < 5.25) then
        #PA("EWContInsR") + 1/(0.75/(#PA("EWInsR")+0.9)+0.25/6.88)   
     else #PA("EWContInsR") + 1/(0.75/#PA("EWInsR")+0.25/6.88)
      endif endif }
   ..

$ Attic floor Frame factor = 0.10
$ Edge factor = 0.05, applied above joist only
$ assume 3.5 inch wood thickness, any additional insulation is above rafter
Mat_InsLay_AttFlr = MATERIAL        
   TYPE             = RESISTANCE
   RESISTANCE       = {
      if (#PA("AttFlrInsR")/3.77 > 3.5) then
        1/(0.9/(3.5*3.77) + 0.1/(3.5*1.25))    $ joists + cavity fill
        + (#PA("AttFlrInsR")/3.77 - 3.5) * 3.77/1.05  $ + above joists
      else if (#PA("AttFlrInsR") == 0) then 1/(0.9/0.9 + 0.1/(3.5*1.25))  $ joist/cavity only
      else 1/(0.9/#PA("AttFlrInsR") + 0.1/(3.5*1.25)) endif endif } $ joist/cavity only
   ..

$ Floor above crawl frame factor = 0.10
$ Cavity gap R = 1.0, Joist is 1.5 x 11.25
Mat_InsLay_FlrAbvCrawl = MATERIAL        
   TYPE             = RESISTANCE
   RESISTANCE       = {
      if (#PA("FlrInsR")/#PA("FlrRperInch") < 11.25) then
        1/(0.9/(#PA("FlrInsR")+1.0)+0.1/(11.25*1.25))   
      else 1/(0.9/#PA("FlrInsR")+0.1/(11.25*1.25)) endif }
   ..

SET-DEFAULT FOR WINDOW
   GLASS-TYPE       = "GlassType_GTC"
   C-UNIT-H        = { #PA("WinHt") }
$   C-UNIT-W         = {#G("PROJECT-DATA","TYP-WIN-WIDTH")}
   Y             =  { #PA("WallHt") - #PA("WinHt") - 1 } 
   FRAME-CONDUCT = {1 / (1/#PA("FrameU") - 0.197)}
   FRAME-ABS = 0.7
   FRAME-WIDTH = {(#L("C-UNIT-H")+#L("C-UNIT-W")
      -SQRT((#L("C-UNIT-H")+#L("C-UNIT-W"))**2
     -4*#L("C-UNIT-H")*#L("C-UNIT-W")*#PA("WinFrmFrac")))/4}
   HEIGHT = {#L("C-UNIT-H") - 2*#L("FRAME-WIDTH")}
   WIDTH = { #L("C-UNIT-W") - 2*#L("FRAME-WIDTH")}   
   WIN-SHADE-TYPE   = FIXED-INTERIOR
   SHADING-SCHEDULE = "D_ResWinShad_Yr"
   ..


$ ---------------------------------------------------------
$   Polygons
$ ---------------------------------------------------------

"EL1 Floor Polygon" = POLYGON         
   V1               = ( 0   , 0    )
   V2             = ( { #PA("FlrLen") }  , 0    )
   V3             = ( { #PA("FlrLen") }  , 23    )
   V4               = ( 0   , 23   )
   ..
"EL1 Space Polygon 1" = POLYGON         
   V1               = ( 0   , 0    )
   V2             = ( { #PA("LivLen") }  , 0    )
   V3             = ( { #PA("LivLen") }  , 23    )
   V4               = ( 0   , 23   )
   ..
"EL1 Space Polygon 2" = POLYGON         
   V1               = ( 0   , 0    )
   V2               = ( 23  , 0    )
   V3             = ( 23, { #PA("BrmLen") }  )
   V4             = ( 0, { #PA("BrmLen") }  )
   ..
"EL2 Floor Polygon" = POLYGON         
   V1               = ( 0   , 0    )
   V2             = ( { #PA("FlrLen") }  , 0    )
   V3             = ( { #PA("FlrLen") }  , 23    )
   V4               = ( 0   , 23   )
   .. 
"EL2 Space Polygon 1" = POLYGON         
   V1               = ( 0   , 0    )
   V2             = ( { #PA("LivLen") }  , 0    )
   V3             = ( { #PA("LivLen") }  , 23    )
   V4               = ( 0   , 23   )
   ..
"EL2 Space Polygon 2" = POLYGON         
   V1               = ( 0   , 0    )
   V2               = ( 23  , 0    )
   V3             = ( 23, { #PA("BrmLen") }  )
   V4             = ( 0, { #PA("BrmLen") }  )
   ..

House1Awning1 = BUILDING-SHADE
   SHADE-SCHEDULE   = "DEER Res Monthly Shade Sched"
   X = { 54 + 1 * #PA("DLen") }
   Y = 0
   Z = 10
   HEIGHT = 10
   WIDTH = { 54 + 1 * #PA("DLen") }
   AZIMUTH          = 0
   TILT             = 0
   SHAPE            = RECTANGLE
   ..
House1Awning2 = BUILDING-SHADE
   SHADE-SCHEDULE   = "DEER Res Monthly Shade Sched"
   X = { 54 + 1 * #PA("DLen") }
   Y = 33
   Z = 10
   HEIGHT = 10
   WIDTH = { 54 + 1 * #PA("DLen") }
   AZIMUTH          = 0
   TILT             = 0
   SHAPE            = RECTANGLE
   ..
House1OHang1 = BUILDING-SHADE
   X = { 56 + 1 * #PA("DLen") }
   Y = -10
   Z = 10
   HEIGHT = 2
   WIDTH = 43
   AZIMUTH          = 90
   TILT             = 0
   SHAPE            = RECTANGLE
   ..
House1OHang2 = BUILDING-SHADE
   X = 0
   Y = -10
   Z = 10
   HEIGHT = 2
   WIDTH = 43
   AZIMUTH          = 90
   TILT             = 0
   SHAPE            = RECTANGLE
   ..
House1Shade1 = BUILDING-SHADE
   X = { 54 + 1 * #PA("DLen") }
   Y = -30
   Z = 0
   HEIGHT = 15
   WIDTH = { 56 + 1 * #PA("DLen") }
   AZIMUTH          = 0
   TILT             = 90
   SHAPE            = RECTANGLE
   ..
House1Shade2 = BUILDING-SHADE
   X = { 54 + 1 * #PA("DLen") }
   Y = 53
   Z = 0
   HEIGHT = 15
   WIDTH = { 56 + 1 * #PA("DLen") }
   AZIMUTH          = 0
   TILT             = 90
   SHAPE            = RECTANGLE
   ..
House2Awning1 = BUILDING-SHADE
   SHADE-SCHEDULE   = "DEER Res Monthly Shade Sched"
   X = { 48.5 + 0.5 * #PA("DLen") }
   Y = { 107.5 + -0.5 * #PA("DLen") }
   Z = 10
   HEIGHT = 10
   WIDTH = { 54 + 1 * #PA("DLen") }
   AZIMUTH          = 90
   TILT             = 0
   SHAPE            = RECTANGLE
   ..
House2Awning2 = BUILDING-SHADE
   SHADE-SCHEDULE   = "DEER Res Monthly Shade Sched"
   X = { 15.5 + 0.5 * #PA("DLen") }
   Y = { 107.5 + -0.5 * #PA("DLen") }
   Z = 10
   HEIGHT = 10
   WIDTH = { 54 + 1 * #PA("DLen") }
   AZIMUTH          = 90
   TILT             = 0
   SHAPE            = RECTANGLE
   ..
House2OHang1 = BUILDING-SHADE
   X = { 48.5 + 0.5 * #PA("DLen") }
   Y = { 107.5 + -0.5 * #PA("DLen") }
   Z = 10
   HEIGHT = 2
   WIDTH = 43
   AZIMUTH          = 0
   TILT             = 0
   SHAPE            = RECTANGLE
   ..
House2OHang2 = BUILDING-SHADE
   X = { 48.5 + 0.5 * #PA("DLen") }
   Y = { 163.5 + 0.5 * #PA("DLen") }
   Z = 10
   HEIGHT = 2
   WIDTH = 43
   AZIMUTH          = 0
   TILT             = 0
   SHAPE            = RECTANGLE
   ..
House2Shade1 = BUILDING-SHADE
   X = 67.5
   Y = { 107.5 + -0.5 * #PA("DLen") }
   Z = 0
   HEIGHT = 15
   WIDTH = { 56 + 1 * #PA("DLen") }
   AZIMUTH          = 90
   TILT             = 90
   SHAPE            = RECTANGLE
   ..
House2Shade2 = BUILDING-SHADE
   X = -15.5
   Y = { 107.5 + -0.5 * #PA("DLen") }
   Z = 0
   HEIGHT = 15
   WIDTH = { 56 + 1 * #PA("DLen") }
   AZIMUTH          = 90
   TILT             = 90
   SHAPE            = RECTANGLE
   ..

$ ======== SPACE GEOMETRY =================================

"EL1 Ground Flr" = FLOOR           
   AZIMUTH          = 360
   POLYGON          = "EL1 Floor Polygon"
   SHAPE            = POLYGON
   FLOOR-HEIGHT     = { #PA("WallHt") }
   SPACE-HEIGHT     = { #PA("ClgHt") }
   C-DIAGRAM-DATA   = *DBLWD-1 Diag Data*
   C-NUM-STORIES = 1
   ..
"EL1 West Perim Spc (G.W1)" = SPACE           
   SHAPE            = POLYGON
   ZONE-TYPE        = CONDITIONED
   POLYGON          = "EL1 Space Polygon 1"
   LOCATION         = FLOOR-V1
   C-ACTIVITY-DESC  = "ResLiving"
   ##include IncFiles\Res_Liv_Space.inp
   ..
"EL1 South Wall (G.W1.E1)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL1 South Win (G.W1.E1.W1)" = WINDOW          
   X                = 8
   Y                = 3
   C-UNIT-W            = { #PA("WinWid1") }
   ..
"EL1 South Door (G.W1.E1.D1)" = DOOR            
   CONSTRUCTION = { #SI(#PA("DoorCons"),"DOOR","CONSTRUCTION") }
   X                = 21
   HEIGHT           = 6.7
   WIDTH            = 3
   ..
"EL1 North Wall (G.W1.E2)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V3
   SHADING-SURFACE  = YES
   ..
"EL1 North Win (G.W1.E2.W1)" = WINDOW          
   X                = 12
   Y                = 3
   C-UNIT-W            = { #PA("WinWid1") }
   ..
"EL1 West Wall (G.W1.E3)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL1 West Win (G.W1.E3.W1)" = WINDOW          
   X                = 8
   Y                = 3
   C-UNIT-W       = { #PA("WinWid2") }
   ..
"EL1 Flr (G.W1.I1)" = INTERIOR-WALL   
   NEXT-TO          = "EL1 West Perim CrS (G.W1)"
   CONSTRUCTION     = "Cons_Crawl_Abv"
   LOCATION         = BOTTOM
   ..
"EL1 East Wall (G.W1.I2)" = INTERIOR-WALL   
   NEXT-TO          = "EL1 East Perim Spc (G.E2)"
   CONSTRUCTION     = "Cons_IWallLite"
   LOCATION         = SPACE-V2
   ..
"EL1 Ceiling (G.W1.I3)" = INTERIOR-WALL   
   NEXT-TO          = "EL1 Attc (G.3)"
   CONSTRUCTION     = "Cons_AtticFlr"
   LOCATION         = TOP
   ..
$ Int wall for mass 
A_IntLiv1 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V1
   Y = { 23/2 + 4 }
   CONSTRUCTION = Cons_IWallLite
   ..
A_IntLiv2 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V1
   Y = { 23/2 - 4 }
   CONSTRUCTION = Cons_IWallLite
   ..
A_IntLiv3 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V4
   X = { #PA("LivLen")/2 }
   CONSTRUCTION = Cons_IWallLite
   ..

"EL1 West Perim CrS (G.W1)" = SPACE           
   Z                = -3
   HEIGHT           = 3
   SHAPE            = POLYGON
   ZONE-TYPE        = UNCONDITIONED
   POLYGON          = "EL1 Space Polygon 1"
   LOCATION         = FLOOR-V1
   FLOOR-WEIGHT     = 0
   FURNITURE-TYPE   = LIGHT
   FURN-FRACTION    = 0.4
   FURN-WEIGHT      = 8
   INF-METHOD       = AIR-CHANGE
   INF-FLOW/AREA    = 0.075
   C-ACTIVITY-DESC  = "Crawl"
   ..
"EL1 South Wall (G.W1.E4)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL1 North Wall (G.W1.E5)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V3
   SHADING-SURFACE  = YES
   ..
"EL1 West Wall (G.W1.E6)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL1 Flr (G.W1.U1)" = UNDERGROUND-WALL
   CONSTRUCTION     = "Cons_Crawl_Flr"
   LOCATION         = BOTTOM
   ..
"EL1 East Perim Spc (G.E2)" = SPACE           
   SHAPE            = POLYGON
   ZONE-TYPE        = CONDITIONED
   POLYGON          = "EL1 Space Polygon 2"
   LOCATION         = FLOOR-V2
   C-ACTIVITY-DESC  = "ResBedroom"
   ##include IncFiles\Res_BRm_Space.inp
   ..
"EL1 East Wall (G.E2.E7)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL1 East Win (G.E2.E7.W1)" = WINDOW          
   X                = 8
   Y                = 3
   C-UNIT-W       = { #PA("WinWid2") }
   ..
"EL1 North Wall (G.E2.E8)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V2
   SHADING-SURFACE  = YES
   ..
"EL1 North Win (G.E2.E8.W1)" = WINDOW          
   X                = 8
   Y                = 3
   C-UNIT-W            = { #PA("WinWid1") }
   ..
"EL1 South Wall (G.E2.E9)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL1 South Win (G.E2.E9.W1)" = WINDOW          
   X                = 4
   Y                = 3
   C-UNIT-W            = { #PA("WinWid1") }
   ..
"EL1 Flr (G.E2.I4)" = INTERIOR-WALL   
   NEXT-TO          = "EL1 East Perim CrS (G.E2)"
   CONSTRUCTION     = "Cons_Crawl_Abv"
   LOCATION         = BOTTOM
   ..
"EL1 Ceiling (G.E2.I5)" = INTERIOR-WALL   
   NEXT-TO          = "EL1 Attc (G.3)"
   CONSTRUCTION     = "Cons_AtticFlr"
   LOCATION         = TOP
   ..
$ Int wall for mass 
A_IntBrm1 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V2
   X = { 23/2 + 4 }
   CONSTRUCTION = Cons_IWallLite
   ..
A_IntBrm2 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V2
   X = { 23/2 - 4 }
   CONSTRUCTION = Cons_IWallLite
   ..
A_IntBrm3 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V1
   Y = { #PA("BrmLen")/2 }
   CONSTRUCTION = Cons_IWallLite
   ..

"EL1 East Perim CrS (G.E2)" = SPACE           
   Z                = -3
   HEIGHT           = 3
   SHAPE            = POLYGON
   ZONE-TYPE        = UNCONDITIONED
   POLYGON          = "EL1 Space Polygon 2"
   LOCATION         = FLOOR-V2
   FLOOR-WEIGHT     = 0
   FURNITURE-TYPE   = LIGHT
   FURN-FRACTION    = 0.4
   FURN-WEIGHT      = 8
   INF-METHOD       = AIR-CHANGE
   INF-FLOW/AREA    = 0.075
   C-ACTIVITY-DESC  = "Crawl"
   ..
"EL1 East Wall (G.E2.E10)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL1 North Wall (G.E2.E11)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V2
   SHADING-SURFACE  = YES
   ..
"EL1 South Wall (G.E2.E12)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL1 Flr (G.E2.U2)" = UNDERGROUND-WALL
   CONSTRUCTION     = "Cons_Crawl_Flr"
   LOCATION         = BOTTOM
   ..
"EL1 Attc (G.3)" = SPACE           
   Z                = { #PA("ClgHt") }
   HEIGHT           = { #PA("WallHt") - #PA("ClgHt") }
   SHAPE            = POLYGON
   ZONE-TYPE        = UNCONDITIONED
   POLYGON          = "EL1 Floor Polygon"
   FLOOR-WEIGHT     = 0
   FURNITURE-TYPE   = LIGHT
   FURN-FRACTION    = 0.4
   FURN-WEIGHT      = 8
   INF-METHOD       = AIR-CHANGE
   INF-FLOW/AREA = {0.35 * #L("HEIGHT")/60}
   C-ACTIVITY-DESC  = "Attic"
   ..
"EL1 South Wall (G.3.E13)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofAtticNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL1 East Wall (G.3.E14)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofAtticNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = SPACE-V2
   SHADING-SURFACE  = YES
   ..
"EL1 North Wall (G.3.E15)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofAtticNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = SPACE-V3
   SHADING-SURFACE  = YES
   ..
"EL1 West Wall (G.3.E16)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofAtticNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL1 Roof (G.3.E17)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofPlywdNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = TOP
   SHADING-SURFACE  = YES
   ..
"EL2 Ground Flr" = FLOOR           
   X                = { 38.5 + #PA("DLen") / 2 }
   Y                = { 107.5 - #PA("DLen") / 2 }
   AZIMUTH          = 270
   POLYGON          = "EL2 Floor Polygon"
   SHAPE            = POLYGON
   FLOOR-HEIGHT     = { #PA("WallHt") }
   SPACE-HEIGHT     = { #PA("ClgHt") }
   C-DIAGRAM-DATA   = *DBLWD-2 Diag Data*
   C-NUM-STORIES = 1
   ..
"EL2 South Perim Spc (G.S1)" = SPACE           
   SHAPE            = POLYGON
   ZONE-TYPE        = CONDITIONED
   POLYGON          = "EL2 Space Polygon 1"
   LOCATION         = FLOOR-V1
   C-ACTIVITY-DESC  = "ResLiving"
   ##include IncFiles\Res_Liv_Space.inp
   ..
"EL2 East Wall (G.S1.E1)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL2 East Win (G.S1.E1.W1)" = WINDOW          
   X                = 8
   Y                = 3
   C-UNIT-W       = { #PA("WinWid1") }
   ..
"EL2 East Door (G.S1.E1.D1)" = DOOR            
   CONSTRUCTION = { #SI(#PA("DoorCons"),"DOOR","CONSTRUCTION") }
   X                = 21
   HEIGHT           = 6.7
   WIDTH            = 3
   ..
"EL2 West Wall (G.S1.E2)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V3
   SHADING-SURFACE  = YES
   ..
"EL2 West Win (G.S1.E2.W1)" = WINDOW          
   X                = 12
   Y                = 3
   C-UNIT-W       = { #PA("WinWid1") }
   ..
"EL2 South Wall (G.S1.E3)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL2 South Win (G.S1.E3.W1)" = WINDOW          
   X                = 8
   Y                = 3
   C-UNIT-W       = { #PA("WinWid2") }
   ..
"EL2 Flr (G.S1.I1)" = INTERIOR-WALL   
   NEXT-TO          = "EL2 South Perim CrS (G.S1)"
   CONSTRUCTION     = "Cons_Crawl_Abv"
   LOCATION         = BOTTOM
   ..
"EL2 North Wall (G.S1.I2)" = INTERIOR-WALL   
   NEXT-TO          = "EL2 North Perim Spc (G.N2)"
   CONSTRUCTION     = "Cons_IWallLite"
   LOCATION         = SPACE-V2
   ..
"EL2 Ceiling (G.S1.I3)" = INTERIOR-WALL   
   NEXT-TO          = "EL2 Attc (G.3)"
   CONSTRUCTION     = "Cons_AtticFlr"
   LOCATION         = TOP
   ..
$ Int wall for mass 
B_IntLiv1 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V1
   Y = { 23/2 + 4 }
   CONSTRUCTION = Cons_IWallLite
   ..
B_IntLiv2 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V1
   Y = { 23/2 - 4 }
   CONSTRUCTION = Cons_IWallLite
   ..
B_IntLiv3 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V4
   X = { #PA("LivLen")/2 }
   CONSTRUCTION = Cons_IWallLite
   ..
"EL2 South Perim CrS (G.S1)" = SPACE           
   Z                = -3
   HEIGHT           = 3
   SHAPE            = POLYGON
   ZONE-TYPE        = UNCONDITIONED
   POLYGON          = "EL2 Space Polygon 1"
   LOCATION         = FLOOR-V1
   FLOOR-WEIGHT     = 0
   FURNITURE-TYPE   = LIGHT
   FURN-FRACTION    = 0.4
   FURN-WEIGHT      = 8
   INF-METHOD       = AIR-CHANGE
   INF-FLOW/AREA    = 0.075
   C-ACTIVITY-DESC  = "Crawl"
   ..
"EL2 East Wall (G.S1.E4)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL2 West Wall (G.S1.E5)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V3
   SHADING-SURFACE  = YES
   ..
"EL2 South Wall (G.S1.E6)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL2 Flr (G.S1.U1)" = UNDERGROUND-WALL
   CONSTRUCTION     = "Cons_Crawl_Flr"
   LOCATION         = BOTTOM
   ..
"EL2 North Perim Spc (G.N2)" = SPACE           
   SHAPE            = POLYGON
   ZONE-TYPE        = CONDITIONED
   POLYGON          = "EL2 Space Polygon 2"
   LOCATION         = FLOOR-V2
   C-ACTIVITY-DESC  = "ResBedroom"
   ##include IncFiles\Res_BRm_Space.inp
   ..
"EL2 North Wall (G.N2.E7)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL2 North Win (G.N2.E7.W1)" = WINDOW          
   X                = 8
   Y                = 3
   C-UNIT-W       = { #PA("WinWid2") }
   ..
"EL2 West Wall (G.N2.E8)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V2
   SHADING-SURFACE  = YES
   ..
"EL2 West Win (G.N2.E8.W1)" = WINDOW          
   X                = 8
   Y                = 3
   C-UNIT-W       = { #PA("WinWid1") }
   ..
"EL2 East Wall (G.N2.E9)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_EWallWoodFrm"
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL2 East Win (G.N2.E9.W1)" = WINDOW          
   X                = 4
   Y                = 3
   C-UNIT-W       = { #PA("WinWid1") }
   ..
"EL2 Flr (G.N2.I4)" = INTERIOR-WALL   
   NEXT-TO          = "EL2 North Perim CrS (G.N2)"
   CONSTRUCTION     = "Cons_Crawl_Abv"
   LOCATION         = BOTTOM
   ..
"EL2 Ceiling (G.N2.I5)" = INTERIOR-WALL   
   NEXT-TO          = "EL2 Attc (G.3)"
   CONSTRUCTION     = "Cons_AtticFlr"
   LOCATION         = TOP
   ..
$ Int wall for mass 
B_IntBrm1 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V2
   X = { 23/2 + 4 }
   CONSTRUCTION = Cons_IWallLite
   ..
B_IntBrm2 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V2
   X = { 23/2 - 4 }
   CONSTRUCTION = Cons_IWallLite
   ..
B_IntBrm3 = INTERIOR-WALL
   INT-WALL-TYPE    = INTERNAL
   LOCATION = SPACE-V1
   Y = { #PA("BrmLen")/2 }
   CONSTRUCTION = Cons_IWallLite
   ..
"EL2 North Perim CrS (G.N2)" = SPACE           
   Z                = -3
   HEIGHT           = 3
   SHAPE            = POLYGON
   ZONE-TYPE        = UNCONDITIONED
   POLYGON          = "EL2 Space Polygon 2"
   LOCATION         = FLOOR-V2
   FLOOR-WEIGHT     = 0
   FURNITURE-TYPE   = LIGHT
   FURN-FRACTION    = 0.4
   FURN-WEIGHT      = 8
   INF-METHOD       = AIR-CHANGE
   INF-FLOW/AREA    = 0.075
   C-ACTIVITY-DESC  = "Crawl"
   ..
"EL2 North Wall (G.N2.E10)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL2 West Wall (G.N2.E11)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V2
   SHADING-SURFACE  = YES
   ..
"EL2 East Wall (G.N2.E12)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_Crawl_Wall"
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL2 Flr (G.N2.U2)" = UNDERGROUND-WALL
   CONSTRUCTION     = "Cons_Crawl_Flr"
   LOCATION         = BOTTOM
   ..
"EL2 Attc (G.3)" = SPACE           
   Z                = { #PA("ClgHt") }
   HEIGHT           = { #PA("WallHt") - #PA("ClgHt") }
   SHAPE            = POLYGON
   ZONE-TYPE        = UNCONDITIONED
   POLYGON          = "EL2 Floor Polygon"
   FLOOR-WEIGHT     = 0
   FURNITURE-TYPE   = LIGHT
   FURN-FRACTION    = 0.4
   FURN-WEIGHT      = 8
   INF-METHOD       = AIR-CHANGE
   INF-FLOW/AREA = {0.35 * #L("HEIGHT")/60}
   C-ACTIVITY-DESC  = "Attic"
   ..
"EL2 East Wall (G.3.E13)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofAtticNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = SPACE-V1
   SHADING-SURFACE  = YES
   ..
"EL2 North Wall (G.3.E14)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofAtticNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = SPACE-V2
   SHADING-SURFACE  = YES
   ..
"EL2 West Wall (G.3.E15)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofAtticNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = SPACE-V3
   SHADING-SURFACE  = YES
   ..
"EL2 South Wall (G.3.E16)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofAtticNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = SPACE-V4
   SHADING-SURFACE  = YES
   ..
"EL2 Roof (G.3.E17)" = EXTERIOR-WALL   
   CONSTRUCTION     = "Cons_RoofPlywdNoIns"
   INSIDE-EMISS = {#PA("RoofInsideEmiss")}
   INSIDE-RCONV = {#PA("RoofInsideRConv")}
   LOCATION         = TOP
   SHADING-SURFACE  = YES
   ..



$ *********************************************************
$ **                                                     **
$ **                Performance Curves                   **
$ **                                                     **
$ *********************************************************

DEER_Refg_Power_FT = CURVE-FIT       
   TYPE             = QUADRATIC-T
   INPUT-TYPE       = DATA
   INDEPENDENT      = ( 65, 81.5, 90 )
   DEPENDENT        = ( 0.66, 1, 1.17 )
   ..


$ *********************************************************
$ **                                                     **
$ **              Electric & Fuel Meters                 **
$ **                                                     **
$ *********************************************************

$ ---------------------------------------------------------
$              Electric Meters
$ ---------------------------------------------------------

EM1 = ELEC-METER                              $master meter
    TYPE             = UTILITY      ..


$ ---------------------------------------------------------
$              Fuel Meters
$ ---------------------------------------------------------

FM1 = FUEL-METER                               $master meter
    TYPE             = NATURAL-GAS  ..


$ ---------------------------------------------------------
$              Master Meters
$ ---------------------------------------------------------

MASTER_METERS = MASTER-METERS   
   MSTR-ELEC-METER  = "EM1"
   MSTR-FUEL-METER  = "FM1"
   ..

DHW_Circ_H1  = CIRCULATION-LOOP
   TYPE             = DHW
   DESIGN-HEAT-T    = 135
   PROCESS-FLOW     = ( 0.347, 0.0973, 0.389,
     {#PA("DWGalPerCycle")*#PA("DWCyclesPerYr")/(365*60)}, 
     {#PA("CWGalPerYr")*#PA("CWCyclesPerYr")
        /(#PA("CWRefCyclesPerYr")*365*60)} )
   PROCESS-SCH = ( "D_DMo_All_DHW_Sink_Yr", 
                   "D_DMo_All_DHW_Bath_Yr", 
                   "D_DMo_All_DHW_Shwr_Yr", 
                   "D_DMo_All_DHW_DWshr_Yr", 
                   "D_DMo_All_DHW_CWshr_Yr" )
   PROCESS-T        = ( 105, 105, 105, 135, 135 )
   ..
DHW_Circ_H2  = CIRCULATION-LOOP
   TYPE             = DHW
   DESIGN-HEAT-T    = 135
   PROCESS-FLOW     = ( 0.347, 0.0973, 0.389,
     {#PA("DWGalPerCycle")*#PA("DWCyclesPerYr")/(365*60)}, 
     {#PA("CWGalPerYr")*#PA("CWCyclesPerYr")
        /(#PA("CWRefCyclesPerYr")*365*60)} )
   PROCESS-SCH = ( "D_DMo_All_DHW_Sink_Yr", 
                   "D_DMo_All_DHW_Bath_Yr", 
                   "D_DMo_All_DHW_Shwr_Yr", 
                   "D_DMo_All_DHW_DWshr_Yr", 
                   "D_DMo_All_DHW_CWshr_Yr" )
   PROCESS-T        = ( 105, 105, 105, 135, 135 )
   ..

DHW_Htr_H1  = DW-HEATER
   TYPE = mDhwFuel_Type[]
   DHW-LOOP = DHW_Circ_H1
   LOCATION = OUTDOOR
   TANK-VOLUME      = {#PA("WH_Rated_Gallons")}
   CAPACITY         = {#PA("DhwInpRateBtuh")/1000000}
   HEAT-INPUT-RATIO = { if(#PA("DhwFuel_Type")=="GAS") then
      #PA("DHW_HIR") else unused endif }
   HIR-FPLR         = { if(#PA("DhwFuel_Type")=="GAS") then
      #SI("DEER DHW HIR-FPLR Curve","DW-HEATER","HIR-FPLR") 
      else unused endif }
   ELEC-INPUT-RATIO = { if(#PA("DhwFuel_Type")=="ELEC") then
      #PA("DHW_EIR") else unused endif }
   TANK-UA          = {#PA("DhwTankUA")}
   ..
DHW_Htr_H2  = DW-HEATER
   TYPE = mDhwFuel_Type[]
   DHW-LOOP = DHW_Circ_H2
   LOCATION = OUTDOOR
   TANK-VOLUME      = {#PA("WH_Rated_Gallons")}
   CAPACITY         = {#PA("DhwInpRateBtuh")/1000000}
   HEAT-INPUT-RATIO = { if(#PA("DhwFuel_Type")=="GAS") then
      #PA("DHW_HIR") else unused endif }
   HIR-FPLR         = { if(#PA("DhwFuel_Type")=="GAS") then
      #SI("DEER DHW HIR-FPLR Curve","DW-HEATER","HIR-FPLR") 
      else unused endif }
   ELEC-INPUT-RATIO = { if(#PA("DhwFuel_Type")=="ELEC") then
      #PA("DHW_EIR") else unused endif }
   TANK-UA          = {#PA("DhwTankUA")}
   ..


SET-DEFAULT FOR SYSTEM
       ##if #[CoolType[] EQS DX]
         ##include IncFiles\SystemResDX.inp
       ##else
         ##include IncFiles\SystemResNC.inp
       ##endif

   DUCT-AIR-LOSS    = {#PA("DuctInAttic1")
      * #PA("DuctAirLoss") * #PA("SupplyAirLossFrac") }
   DUCT-AIR-LOSS-OA = {#PA("DuctAirLossOA")}
   DUCT-UA          = { 1 * #G("PROJECT-DATA","AREA-PER-FLR")
      *0.27*(1.35-0.35 * 1)/#PA("DuctRValue")}
   RETURN-UA        = { 1 * #G("PROJECT-DATA","AREA-PER-FLR")
      * 0.05 * 1 /#PA("DuctRValue")}

$     Ventilation
   VENT-METHOD = {#SIT(#PA("VentMethod"),"SYSTEM","VENT-METHOD",#L("TYPE"))}
   VENT-TEMP-SCH    = "D_ResNatVentTemp_Yr"
   NATURAL-VENT-AC  = {#PA("NatVentACH")}
   NATURAL-VENT-SCH = #[ "D_ResNatVentOn" // #[ BldgLoc[] // "_Yr" ]]
   OPEN-VENT-SCH    = #[ "D_ResNatVentOpen_" // #[ BldgLoc[] // "_Yr" ]]
   FAN-VENT-AC      = {#PA("FanVentCFMpSF")*60/#PA("WallHt")}
   VENT-MAX-T       = {#PA("VentMaxT")}
   NATURAL-VENT-KW = {if(#PA("VentFanWperCFM") > 0.001) then
      #PA("VentFanWperCFM")*#G("PROJECT-DATA","AREA-PER-FLR")  
      * #PA("FanVentCFMpSF") * 0.001 else no_default endif }

   COOLING-CAPACITY = {1*12000*#G("PROJECT-DATA","AREA-PER-FLR") 
      *#PA("TotCoolCapMult") / #PA("CoolCapSfPerTon")}
   HEATING-CAPACITY = {-1*1000*#G("PROJECT-DATA","AREA-PER-FLR") 
       / #PA("HtgAreaPerKbtuh")}
       ##if #[CoolType[] NES DX]
   SUPPLY-FLOW    = {1000*1*#G("PROJECT-DATA","AREA-PER-FLR") 
      *#PA("CfmPerBtuh") / #PA("HtgAreaPerKbtuh")}
       ##endif
   ..

SET-DEFAULT FOR ZONE                            
   TYPE = CONDITIONED
   DESIGN-HEAT-T    = 72
   DESIGN-COOL-T    = 75
   HEAT-TEMP-SCH    = #[ "ResHeatTstat" // BldgLoc[] ]
       ##if #[CoolType[] EQS DX]
   COOL-TEMP-SCH    = #[ "ResCoolTstat" // BldgLoc[] ]
       ##endif
       ##if #[HeatType[] EQS ELECTRIC]
   BASEBOARD-RATING =  {-1000*#LR("SPACE","AREA") 
       / #PA("HtgAreaPerKbtuh")}
   BASEBOARD-CTRL   = THERMOSTATIC
       ##endif
   ..                                       


"EL1 Sys1 (PVVT) (G)"  = SYSTEM
       ##if #[CoolType[] EQS DX]
   TYPE = PVVT
       ##else
   TYPE = PVVT
       ##endif
   CONTROL-ZONE = "EL1 West Perim Zn (G.W1)"
   DUCT-ZONE = "EL1 At Zn (G.3)"
   C-SYS-ZONE-CONFG = SINGLE-ZONE
   ..
"EL1 West Perim Zn (G.W1)"  = ZONE
   TYPE = CONDITIONED
   SPACE = "EL1 West Perim Spc (G.W1)"
   ..
"EL1 West Perim CrZ (G.W1)"  = ZONE
   TYPE = UNCONDITIONED
   SPACE = "EL1 West Perim CrS (G.W1)"
   ..
"EL1 East Perim Zn (G.E2)"  = ZONE
   TYPE = CONDITIONED
   SPACE = "EL1 East Perim Spc (G.E2)"
   ..
"EL1 East Perim CrZ (G.E2)"  = ZONE
   TYPE = UNCONDITIONED
   SPACE = "EL1 East Perim CrS (G.E2)"
   ..
"EL1 At Zn (G.3)"  = ZONE
   TYPE = UNCONDITIONED
   SPACE = "EL1 Attc (G.3)"
   ..

"EL2 Sys1 (PVVT) (G)"  = SYSTEM
       ##if #[CoolType[] EQS DX]
   TYPE = PVVT
       ##else
   TYPE = PVVT
       ##endif
   CONTROL-ZONE = "EL2 South Perim Zn (G.S1)"
   DUCT-ZONE = "EL2 At Zn (G.3)"
   C-SYS-ZONE-CONFG = SINGLE-ZONE
   ..
"EL2 South Perim Zn (G.S1)"  = ZONE
   TYPE = CONDITIONED
   SPACE = "EL2 South Perim Spc (G.S1)"
   ..
"EL2 South Perim CrZ (G.S1)"  = ZONE
   TYPE = UNCONDITIONED
   SPACE = "EL2 South Perim CrS (G.S1)"
   ..
"EL2 North Perim Zn (G.N2)"  = ZONE
   TYPE = CONDITIONED
   SPACE = "EL2 North Perim Spc (G.N2)"
   ..
"EL2 North Perim CrZ (G.N2)"  = ZONE
   TYPE = UNCONDITIONED
   SPACE = "EL2 North Perim CrS (G.N2)"
   ..
"EL2 At Zn (G.3)"  = ZONE
   TYPE = UNCONDITIONED
   SPACE = "EL2 Attc (G.3)"
   ..


$ *********************************************************
$ **                                                     **
$ **                 Output Reporting                    **
$ **                                                     **
$ *********************************************************

$ ---------------------------------------------------------
$              Loads Non-Hourly Reporting
$ ---------------------------------------------------------

LOADS-REPORT    
   VERIFICATION     = ( ALL-VERIFICATION )
   SUMMARY          = ( ALL-SUMMARY )
   ..


$ ---------------------------------------------------------
$              Systems Non-Hourly Reporting
$ ---------------------------------------------------------

SYSTEMS-REPORT  
   VERIFICATION     = ( ALL-VERIFICATION )
   SUMMARY          = ( ALL-SUMMARY )
   HOURLY-DATA-SAVE = FORMATTED
   ..

HrlyRepDayOff = DAY-SCHEDULE-PD TYPE = ON/OFF VALUES = (0)  ..
HrlyRepDayOn = DAY-SCHEDULE-PD TYPE = ON/OFF VALUES = (1)  ..

HrlyRepWkOn = WEEK-SCHEDULE-PD
   TYPE             = ON/OFF
   DAY-SCHEDULES    = ( HrlyRepDayOn, &D, &D, &D, &D, &D, &D, &D,  
         HrlyRepDayOff, HrlyRepDayOff )   ..

HrlyRepWkOff = WEEK-SCHEDULE-PD
   TYPE          = ON/OFF
   DAY-SCHEDULES = ( HrlyRepDayOff, &D,&D,&D,&D,&D,&D,&D,&D,&D )
   ..
HrlyRepSch = SCHEDULE TYPE = ON/OFF   
   THRU DEC 31 HrlyRepWkOn 
   ..

EM1_OnPeakKW_RB = REPORT-BLOCK    
   VARIABLE-TYPE    = "EM1"
      ##if #[EU_HrRepVar[] EQ 0]
   VARIABLE-LIST    = ( 20 )
      ##else
   VARIABLE-LIST    = ( 20, EU_HrRepVar[] )
      ##endif
   ..

PeakPerKW_HrlyRpt = HOURLY-REPORT   
   REPORT-SCHEDULE  = "HrlyRepSch"
   REPORT-BLOCK     = ( "EM1_OnPeakKW_RB")
   ..

$ ---------------------------------------------------------
$              Plant Non-Hourly Reporting
$ ---------------------------------------------------------

PLANT-REPORT    
   VERIFICATION     = ( ALL-VERIFICATION )
   SUMMARY          = ( ALL-SUMMARY )
   ..


$ ---------------------------------------------------------
$              Economics Non-Hourly Reporting
$ ---------------------------------------------------------

ECONOMICS-REPORT
   VERIFICATION     = ( ALL-VERIFICATION )
   SUMMARY          = ( ALL-SUMMARY )
   ..


$ ---------------------------------------------------------
$              Hourly Reporting
$ ---------------------------------------------------------


$"D08 Hourly Rpt (double meters)" = HOURLY-REPORT   
$   LIBRARY-ENTRY "D08 Hourly Rpt (double meters)"
$   ..


END ..
COMPUTE ..
STOP ..


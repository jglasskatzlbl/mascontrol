$ ======================= Single Compressor Systems ===========================
$ (SnglComp.inc)
$ called by GrocRfg1.inc

$ Defines supermarket single-compressor refrigeration systems with 
$ separate air-cooled condensers for low and medium temperature
$ suction groups,

SET-DEFAULT FOR REFG-SYSTEM
   ALARM-MAX-T = 150
   ..

$ ---------------------------- Single System Compressors ----------------------

$ These curves represent a "typical" compressor in a given temperature
$ application, and are rescaled as required for the application

$ MT suction-groups

3DS3A150E-TFC_flow = CURVE-FIT
   TYPE             = FLOW-FSST&SDT
   ORDER-OF-FIT     = CUBIC
   INPUT-TYPE       = COEFFICIENTS
   MIN-SST = -5       MAX-SST = 50
   MIN-SDT = 65       MAX-SDT = 140
   COEF = (2137.072, 51.90935, -8.34197, 0.572635, -0.16753, 0.043661,
           0.003454, -0.0019, 0.000454, -0.0001)
   ..

3DS3A150E-TFC_pwr = CURVE-FIT
   TYPE             = PWR-FSST&SDT
   ORDER-OF-FIT     = CUBIC
   INPUT-TYPE       = COEFFICIENTS
   MIN-SST = -5       MAX-SST = 50
   MIN-SDT = 65       MAX-SDT = 140
   COEF = (3.465656, -0.0616701, 0.1056851, -0.00190514, 0.002063418,
          -0.00024232, -0.00000464, 0.000015084, -0.00000324, 0.00000012)
   ..

Reed-3DS3A150E-TFC_pwr = CURVE-FIT
   TYPE             = PWR-FSST&SDT
   ORDER-OF-FIT     = CUBIC
   INPUT-TYPE       = COEFFICIENTS
   MIN-SST = -5       MAX-SST = 50
   MIN-SDT = 65       MAX-SDT = 140
   COEF = (3.687457984, -0.065616986, 0.112448946, -0.002027069, 0.002195477,
          -0.000257828, -0.000004937, 0.000016049, -0.000003447, 0.000000128)
   ..

$ LT suction-groups

4DL3A150E-TSE_flow = CURVE-FIT
   TYPE             = FLOW-FSST&SDT
   ORDER-OF-FIT     = CUBIC
   INPUT-TYPE       = COEFFICIENTS
   MIN-SST = -40      MAX-SST = 10
   MIN-SDT = 65       MAX-SDT = 130
   COEF = (7078.273, 101.1925, -126.793, 0.804942, -0.87333, 1.100075,
           0.005186, -0.00202, 0.003768, -0.00323)
   ..

4DL3A150E-TSE_pwr = CURVE-FIT
   TYPE             = PWR-FSST&SDT
   ORDER-OF-FIT     = CUBIC
   INPUT-TYPE       = COEFFICIENTS
   MIN-SST = -40      MAX-SST = 10
   MIN-SDT = 65       MAX-SDT = 130
   COEF = (32.55598, 0.3510652, -0.639569, 0.002127672, -0.00532377,
           0.006745286, 0.000021447, -0.00001388, 0.000033031, -0.00002054)
   ..

Reed-4DL3A150E-TSE_pwr = CURVE-FIT
   TYPE             = PWR-FSST&SDT
   ORDER-OF-FIT     = CUBIC
   INPUT-TYPE       = COEFFICIENTS
   MIN-SST = -40      MAX-SST = 10
   MIN-SDT = 65       MAX-SDT = 130
   COEF = (34.639562720, 0.373533373, -0.680501416, 0.002263843, -0.005664491,
           0.007176984, 0.000022820, -0.000014768, 0.000035145, -0.000021855)
   ..

no_unload_fplr = CURVE-FIT
   TYPE             = QUADRATIC
   MIN-INPUT-1      = 0.0
   MAX-INPUT-1      = 1.1
   INPUT-TYPE       = COEFFICIENTS
   COEF = (0.0, 1.0, 0.0)
   ..

$ ---------------------------- Refg-System ------------------------------------

SingleComp_System = REFG-SYSTEM
   REFRIGERANT      = Refrigerant[]
   DESIGN-WETBULB   = DesignWB[]
   DESIGN-SCT       = 105         $ Held constant since condenser and
                                  $ compressors explicitly sized
   SCT-CTRL         = DRYBULB-STAGED 
   BACKFLOOD-CTRL   = FIXED
   BACKFLOOD-SETPT  = 93
   ..

$ ---------------------------- Condenser --------------------------------------

SingleComp_Condenser = CONDENSER
   TYPE             = AIR-COOLED
   RATED-CAPACITY   = AirCondSCCap&EIR[1250000]
   NUMBER-OF-FANS   = 8
   FAN-STAGE-T      = (-100., -100., 45., 45., 60., 60., 70., 70.)

   REFG-SYSTEM      = SingleComp_System
   RATED-REFG-CLASS = HALOCARBON
   CAPACITY-CTRL    = CYCLE-FAN 
   EQUIPMENT-REPORT = YES
   ..  

$ ---------------------------- Prep Suction Group -----------------------------

Prep_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

Prep_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[32, 6] 
   DISCHARGE-CIRCUI = Prep_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

Prep_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 3590
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = Prep_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

Prep_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = Prep_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

Prep_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- Cooler Suction Group ---------------------------

Cooler_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

Cooler_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[23, 6]
   DISCHARGE-CIRCUI = Cooler_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

Cooler_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 3210
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = Cooler_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

Cooler_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = Cooler_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

Cooler_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseF Suction Group ----------------------------

CaseF_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseF_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[16, 6]
   DISCHARGE-CIRCUI = CaseF_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseF_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2500
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseF_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseF_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseF_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseF_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseG Suction Group ----------------------------

CaseG_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseG_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[18, 6]
   DISCHARGE-CIRCUI = CaseG_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseG_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2440
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseG_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseG_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseG_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseG_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseH Suction Group ----------------------------

CaseH_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseH_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[21, 6]
   DISCHARGE-CIRCUI = CaseH_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseH_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2680
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseH_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseH_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseH_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseH_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseI Suction Group ----------------------------

CaseI_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseI_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[18, 6]
   DISCHARGE-CIRCUI = CaseI_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseI_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2440
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseI_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseI_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseI_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseI_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseJ Suction Group ----------------------------

CaseJ_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseJ_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[21, 6]
   DISCHARGE-CIRCUI = CaseJ_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseJ_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2680
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseJ_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseJ_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseJ_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseJ_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseK Suction Group ----------------------------

CaseK_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseK_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[20, 6]
   DISCHARGE-CIRCUI = CaseK_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseK_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2680
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseK_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseK_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseK_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseK_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseL Suction Group ----------------------------

CaseL_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseL_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[20, 6]
   DISCHARGE-CIRCUI = CaseL_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseL_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2680
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseL_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseL_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseL_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseL_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseM Suction Group ----------------------------

CaseM_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseM_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[20, 6]
   DISCHARGE-CIRCUI = CaseM_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseM_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2680
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseM_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseM_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseM_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseM_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseN Suction Group ----------------------------

CaseN_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseN_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[22, 6]
   DISCHARGE-CIRCUI = CaseN_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseN_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 2870
   RATED-SDT        = 95
   CAP-FSST&SDT     = 3DS3A150E-TFC_flow
   PWR-FSST&SDT     = CompPwr[Reed-3DS3A150E-TFC_pwr, 3DS3A150E-TFC_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseN_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseN_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseN_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseN_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- Freezer Suction Group ---------------------------

Freezer_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

Freezer_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[-14, 6] 
   DISCHARGE-CIRCUI = Freezer_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

Freezer_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 1580
   RATED-SDT        = 95
   CAP-FSST&SDT     = 4DL3A150E-TSE_flow
   PWR-FSST&SDT     = CompPwr[Reed-4DL3A150E-TSE_pwr, 4DL3A150E-TSE_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = Freezer_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

Freezer_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = Freezer_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

Freezer_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseA Suction Group ----------------------------

CaseA_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseA_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[-13, 6]
   DISCHARGE-CIRCUI = CaseA_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseA_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 1660
   RATED-SDT        = 95
   CAP-FSST&SDT     = 4DL3A150E-TSE_flow
   PWR-FSST&SDT     = CompPwr[Reed-4DL3A150E-TSE_pwr, 4DL3A150E-TSE_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseA_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseA_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseA_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseA_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseB Suction Group ----------------------------

CaseB_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseB_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[-13, 6]
   DISCHARGE-CIRCUI = CaseB_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseB_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 1660
   RATED-SDT        = 95
   CAP-FSST&SDT     = 4DL3A150E-TSE_flow
   PWR-FSST&SDT     = CompPwr[Reed-4DL3A150E-TSE_pwr, 4DL3A150E-TSE_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseB_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseB_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseB_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseB_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseC Suction Group ----------------------------

CaseC_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseC_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[-20, 6]
   DISCHARGE-CIRCUI = CaseC_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseC_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 1600
   RATED-SDT        = 95
   CAP-FSST&SDT     = 4DL3A150E-TSE_flow
   PWR-FSST&SDT     = CompPwr[Reed-4DL3A150E-TSE_pwr, 4DL3A150E-TSE_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseC_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseC_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseC_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseC_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseD Suction Group ----------------------------

CaseD_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseD_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[-20, 6]
   DISCHARGE-CIRCUI = CaseD_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseD_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 1330
   RATED-SDT        = 95
   CAP-FSST&SDT     = 4DL3A150E-TSE_flow
   PWR-FSST&SDT     = CompPwr[Reed-4DL3A150E-TSE_pwr, 4DL3A150E-TSE_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseD_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseD_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseD_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseD_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

$ ---------------------------- CaseE Suction Group ----------------------------

CaseE_DischargeLine = REFG-CIRCUIT
   TYPE             = DISCHARGE
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   HOLDBACK-CTRL    = HoldbackCtrl[85, 5]
   EQUIPMENT-REPORT = RfgCircuitRep
   ..  

CaseE_SuctionGroup = SUCTION-GROUP
   TYPE             = PARALLEL
   TEMP-CTRL        = SuctionTempCtrl[-22, 6]
   DISCHARGE-CIRCUI = CaseE_DischargeLine
   EQUIPMENT-REPORT = SuctionGrpRep
   ..  

CaseE_Compressor = COMPRESSOR
   TYPE             = SEMI-HERMETIC
   CAPACITY         = 1330
   RATED-SDT        = 95
   CAP-FSST&SDT     = 4DL3A150E-TSE_flow
   PWR-FSST&SDT     = CompPwr[Reed-4DL3A150E-TSE_pwr, 4DL3A150E-TSE_pwr]
   PWR-FPLR         = no_unload_fplr
   SUCTION-GROUP    = CaseE_SuctionGroup
   EQUIPMENT-REPORT = CompressorRep
   ..

CaseE_SuctionLine = REFG-CIRCUIT
   TYPE             = SUCTION
   ATTACH-TO        = SUCTION-GROUP
   SUCTION-GROUP    = CaseE_SuctionGroup
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

CaseE_LiquidLine = REFG-CIRCUIT
   TYPE             = LIQUID
   ATTACH-TO        = REFG-SYSTEM
   REFG-SYSTEM      = SingleComp_System
   EQUIPMENT-REPORT = RfgCircuitRep
   ..

